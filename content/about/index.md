+++
date = '2012-04-06'
description = 'about'
slug = 'about'
title = 'About'
+++

السلام عليكم ورحمة اللّٰه وبركاته

Halo nama saya Agus, saya seorang Software Engineer disalah satu pusat karir terbesar di jogja. Dan juga seorang suami dari seorang istri.

Sehari-hari saya berkecimpung dengan [PHP](https://php.net/) dan framework [Yii](https://yiiframework.com/), dan juga library [Vuejs](https://vuejs.org/). Selain dari itu saya juga menggunakan bahasa [Go](https://golang.org/) untuk membuat tool-tool yang diperlukan dalam pengembangan aplikasi seperti notifikasi server, sistem publisher subscribe server.

Ada lagi bahasa yang sedang saya gunakan untuk membangun server api yaitu [Elixir](https://elixir-lang.org/), harapan dengan menggunakan `Elixir` aplikasi dapat discale ke level diatas `PHP` dan juga karena nilai jual `Elixir` yang menjanjikan.

Dan mungkin satu lagi bahasa yang saya suka dan ingin mempelajarinya sebagai backup `Go` yaitu [Rust](https://rust-lang.org/) karena sangat menjanjikan dari segi keamanan(safe) dan kecepatan tanpa menggunakan garbage collector seperti pada `Go`.

Jangan lupa kunjungi juga blog saya yang lain di [susilolabs](https://susilolabs.wordpress.com/).
