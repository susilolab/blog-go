---
categories: php
date: "2020-11-09T06:00:00Z"
title: Memeriksa Instansi Klas dengan instanceof
---

![new-mix-project](/img/2020-10-13-instanceof.png)
<!--more-->

Assalamu'alaikum, kali ini saya akan membagi tips singkat bagaimana memeriksa suatu klas itu instance dari klas apa.
Kenapa hal itu penting? atau kenapa kita harus tahu itu?, jawabannya adalah kadang kita butuh kondisi dimana suatu fungsi hanya boleh
berjalan jika suatu klas instace dari klas ini atau itu. Saya beri contoh pada framework Yii 2 dimana fungsi perlu mengetahui
apakah aplikasi sedang berjalan di konsol atau diweb karena pada Yii beberapa komponen tidak dirancang untuk berjalan pada mode
konsol seperti komponen user dan session. Untuk melakukan pengecekan tersebut kita bisa memakai fungsi `instanceof` bawaan dari `PHP`.

{{< highlight php >}}
<?php
class A {}

$a = new A();
if ($a instanceof A) {
    echo "Instanceof A\n";
} else {
    echo "Bukan instanceof dari A\n";
}
{{< / highlight >}}

Fungsi `instanceof` ini sering saya gunakan pada framework Yii 2 di semua kelas yang akan diakses/dijalankan pada mode cli(command line), karena
Yii 2 memiliki extension `yii-queue` di mana extension akan berjalan pada konsol dan pada saat aplikasi konsol berjalan tentu aplikasi tersebut
akan mengakses model-model ataupun komponen. Untuk itu perlu memastikan bahwa model-model atau komponen tersebut tidak mengakses komponen yang dirancang
khusus untuk web seperti `User`, `Session`, `Request`, `Respon` dll. Kalau saya sendiri akan membuang semua fungsi yang berhubungan dengan web
pada model `ActiveRecord` dan mengalihkannya ke file controller.

Sekian, semoga bermanfaat
