---
categories: php
date: "2021-10-15T05:00:00Z"
tags: php
title: Return Objek pada CDbCommand Yii1
---

CDbCommand pada Yii1 defaultnya mengembalikan array pada fungsi-fungsi seperti queryAll dan queryRow. Agar kembaliannya 
berupa objek kita bisa gunakan fungsi `setFetchMode`.
<!--more-->

{{< highlight php >}}
$result = Yii::app()->db->createCommand()
    ->setFetchMode(\PDO::FETCH_OBJ)
    ->select('id, username')
    ->from('user')
    ->where('publish = 1')
    ->queryAll();

foreach ($result as $val) {
    echo $val->id, ' => ', $val->username, "\n";
}
{{< / highlight >}}

Untuk daftar lengkapnya bisa di lihat di web [https://www.php.net/manual/en/pdostatement.fetch.php](https://www.php.net/manual/en/pdostatement.fetch.php)

Sekian
