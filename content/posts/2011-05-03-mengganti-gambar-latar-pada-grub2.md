---
categories: linux
date: "2011-05-03T08:00:00Z"
title: Mengganti gambar latar pada grub2
---

![grub2](/img/grub2.jpg)

Beberapa hari yang lalu saya pernah menulis bagaimana cara mengganti gambar latar/background pada grub2 di linux mint 10.
<!--more-->

Ternyata cara tersebut tidak efektif, setelah saya baca-baca di internet cara meng-customize grub2 akhirnya menemukan cara yang lebih mudah dan aman.

Perhatian:
Backup terlebih file `/etc/grub.d/06_mint_theme` sebelum Anda rubah. Hal ini untuk menghindari sesuatu yang tidak kita inginkan, sehingga mempermudah dalam proses restore.

Berikut ini langkah-langkah mengganti gambar latar pada grub2 di linux mint 10:

Siapkan gambar berukuran 640×480 pixel, tipe gambar boleh png atau jpeg. Dalam hal ini saya menggunakan gambar latar dengan nama `latar_ungu.jpg`.

Buat folder images dan desktop-base di `/usr/share`
{{< highlight bash >}}
$ sudo mkdir /usr/share/images
$ sudo mkdir /usr/share/images/desktop-base 
{{< / highlight >}}

Salin file gambar tersebut ke folder desktop-base
{{< highlight bash >}}
$ sudo cp latar_ungu.jpg /usr/share/images/desktop-base
{{< / highlight >}}

Kemudian edit file `06_mint_theme` di folder `/etc/grub.d`. Cari baris kode berikut dan ganti nama gambar linuxmint dengan latar_ungu dan jangan lupa untu menambahkan ektensi jpg pada script `{png,tga}`.
{{< highlight bash >}}
#sebelum diedit.
for i in {/boot/grub,/usr/share/images/desktop-base}/linuxmint.{png,tga} ; do
#setelah diedit
for i in {/boot/grub,/usr/share/images/desktop-base}/latar_ungu.{png,tga,jpg} ; do
{{< / highlight >}}

Setelah itu jalankan perintah berikut untuk mengupdate grub.
{{< highlight bash >}}
$ sudo update-grub
{{< / highlight >}}

Kemudian restart komputer Anda. Pada langkah kedua Anda juga bisa menggunakan nautilus(file explorer) untuk membuat folder. Caranya tekan tombol keyboard **Alt** dan **F2**. Kemudian ketik perintah `gksu nautilus`, Ketikan password Anda apabila system memintanya. 

Selamat mencoba 🙂
