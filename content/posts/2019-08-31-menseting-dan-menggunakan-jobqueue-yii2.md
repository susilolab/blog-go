---
author: Agus Susilo
categories: php
date: "2019-08-31T18:30:00Z"
title: Menseting dan menggunakan Job Queue pada Yii2
---

Pada Yii2 sudah didukung dengan ektensi job queue yang memungkinkan kita menjalankan tugas secara asynkron via antrian. Job Queue pada yii2 mendukung **DB**, **Redis**, **RabbitMQ**, **AMQP**, **BeansTalk**, **ActiveMQ** dan **Gearman**.
<!--more-->

Saya hanya akan sedikit berbagi job queue dengan redis sebagai backendnya. Saya list apa-apa yang akan kita lakukan.

* Install ektensi yii2-queue
* Seting queue di web.php dan console.php
* Membuat klas tugas
* Menjalankan queue cli

Pertama kita install dulu ektensi yii2-queue

{{< highlight bash >}}
$ php composer.phar require --prefer-dist yiisoft/yii2-queue
{{< / highlight >}}

Kemudian kita perlu memasukan setingan yii2-queue ke file config console dan web

{{< highlight php >}}
// masukan pada file web.php dan console.php
...
'components' => [
    'queue' => [
        'class' => \yii\queue\redis\Queue::class,
        'as log' => \yii\queue\LogBehavior::class,
    ],
],
...
{{< / highlight >}}

Untuk file `console.php` kita juga perlu memasukan queue ke konfig bootstrap seperti ini

{{< highlight php >}}
// console.log
...
'bootstrap' => [
    'queue',
],
...
{{< / highlight >}}

Langkah berikutnya adalah membuat klas job/tugas yang akan kita kirim ke queue yang harus dipisah menjadi klas. Klas ini wajib mendefinisikan fungsi `execute`.

{{< highlight php >}}
<?php
namespace app\jobs;

use yii\base\BaseObject;

class DownloadJob extends BaseObject implements \yii\queue\JobInterface
{
    public $url;
    public $file;

    public function execute($queue)
    {
        file_put_contents($this->file, file_get_contents($this->url));
    }
}
{{< / highlight >}}

Setelah kita membuat klas tugas maka kita dapat mengirim tugas dari mana saja seperti dari model, controller, module, view dll. Cara mengirim tugas ke queue seperti berikut ini:

{{< highlight php >}}
Yii::$app->queue->push(new \app\jobs\Download([
    'url' => 'https://example.com/image.png',
    'file' => '/tmp/image.png'
]));
{{< / highlight >}}

Langkah terakhir yang harus dilakukan adalah menjalankan console queue melalui terminal atau bisa dengan membuat service dilinux. cara menjalankan consolenya seperti dibawah. buka terminal lalu masuk ke folder dimana yii/yii.bat berada.

{{< highlight bash >}}
# jalankan semua queue dan exit setelah tidak ada queue
$ yii queue/run -v

# jalankan semua queue dan loop
$ yii queue/listen -v
{{< / highlight >}}

Dengan adanya job queue akan mengurangi beban webserver dalam memproses request dan job queue tidak terkena timeout dari php. Contoh kasusnya bisa untuk mengirim email ke user secara masal atau mengenerate file xls yang jumlah barisnya ribuan atau pekerjaan yang sifatnya logging.
