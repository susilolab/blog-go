---
categories: elixir
date: "2019-09-03T08:00:00Z"
title: Multi Alias dalam satu baris
---

`Alias` digunakan untuk memperpendek nama modul pada Elixir, bayangkan kamu harus memanggil nama modul yang begitu banyak contoh 
<!--more-->

{{< highlight elixir >}}
defmodule Foo do
    alias Hello.World
    alias Hello.Great
    alias Hello.Print
end
{{< / highlight >}}

Kode alias diatas dapat diganti dengan tanda `{}` sehingga bisa menjadi satu baris

{{< highlight elixir >}}
defmodule Foo do
    alias Hello.{World, Great, Print}
end
{{< / highlight >}}

sekian.
