---
categories: windows
date: "2016-10-01T06:00:00Z"
title: Mengembalikan flash disk yang hilang
---

![disk-manager](/img/001-01-10-2016-flash-disk.png)
<!--more-->

Assalamu’alaikum, di pagi yang cerah ini saya ingin berbagi pengalaman saya mengenai flash disk saya yang hilang. Maksudnya flash disk saya tidak terlihat di windows explorer dan ternyata windows menganggap flash disk saya sebagai removable online padahal kalau dibuka di linux normal-normal saja.

Nah setelah saya mencari solusinya di internet, ternyata flash disk saya masih bisa dikembalikan ke kondisi awal namun kemungkinan besar datanya akan hilang. Cara yang bisa dilakukan untuk mengembalikan flash disk tersebut adalah dengan menggunakan program cli bawaannya windows 7 yaitu diskpart, program ini mungkin ada pada windows 8 dan 10. Pertama buka command prompt dengan user administrator.

![run-cmd-as-administrator](/img/002-01-10-2016-flash-disk.png)

Kemudian jalankan perintah berikut ini

```
C:\> dispart
dispart> list disk
dispart> select disk #
dispart> clean
dispart> create partition primary
dispart> assign
```

Lihat gambar berikut
![dispart](/img/003-01-10-2016-flash-disk.png)

Perintah `list disk` akan memberikan daftar disk yang ada dikomputer, pilihlah disk yang sesuai dengan flash disk Anda. Kemudian ketikkan perintah `select disk 2`, jika flash disk berada pada urutan kedua(sesuaikan dengan disk Anda). Lalu ketikkan perintah `clean`, perlu diingat bahwa perintah `clean` akan menghapus semua data yang ada pada flash disk. Lalu ketik `create partition primary` untuk membuat partisi baru pada flash disk dan yang terakhir ketik perintah `assign`, jika semuanya berjalan lancar maka akan muncul dialog yang meminta Anda untuk memformat flash disk, Silahkan pilih "Format disk" untuk memformat flash disk Anda.

![disk-format](/img/004-01-10-2016-flash-disk.png)

Jika formatnya sukses, selamat flash disk Anda sudah kembali dikenali oleh windows :p. sekian
