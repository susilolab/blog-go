---
categories: elixir
date: "2019-08-31T13:47:42Z"
title: Menambah parameter pada routes helper Phoenix
---

Tips singkat ini saya dapat saat membuat aplikasi todo sederhana dengan phoenix. kasusnya saya menambah custom rute `get /profile/edit/:id` dengan parameter `:id`. awalnya saya pikir harus pakai key dan value.
<!--more-->

{{< highlight erb >}}
<%= link "Edit", to: Routes.user_path(@conn, :edit, id: @user.id) %>
{{< / highlight >}}

Ternyata tidak dikenali oleh phoenix, akhirnya saya hapus key `id` dan sukses.
{{< highlight erb >}}
<%= link "Edit", to: Routes.user_path(@conn, :edit, @user.id) %>
{{< / highlight >}}

sekian.