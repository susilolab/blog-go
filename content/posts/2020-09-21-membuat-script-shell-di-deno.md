---
categories: deno
date: "2020-09-21T05:00:00Z"
tags: 
- deno 
- typescript
title: 'Membuat Script Shell di Deno'
---

Shebang adalah tanda `#!` pada shell script di linux, shell bash/zsh/yang lain menggunakan shebang untuk mendeteksi script tersebut akan dijalankan dengan perintah apa. Awal shell script biasanya seperti ini `#!/usr/bin/env nama_perintah`.
<!--more-->

Contoh
Shell script berikut ini akan dijalankan menggunakan `php`

{{< highlight php >}}
#!/usr/bin/env php
<?php
echo phpversion();
{{< / highlight >}}

Maka jika shell script di atas dijalankan shell akan menggunakan perintah `php` untuk menjalankannya.

Shell script berikut ini akan dijalankan menggunakan `python`

{{< highlight python >}}
#!/usr/bin/env python3

langs = ['PHP', 'Javascript', 'Rust', 'Elixir', 'Go']
for lang in langs:
    print(lang)
{{< / highlight >}}

Maka jika shell script di atas dijalankan shell akan menggunakan perintah `python3` untuk menjalankannya dan seterusnya.

Kasus kali ini agak berbeda ketika saya mencoba membuat shell dengan Deno sebagai executornya. Pernah mencoba membuat shell script agar Deno langsung bisa dijalankan dengan perintah shell seperti ini `./scrip_name` ?. Namun karena Deno ada tambahan perizinan dan command `run` maka jika shell script kita seperti di bawah akan terjadi error.

{{< highlight bash >}}
#!/usr/bin/env deno
{{< / highlight >}}

Bagaimana agar shell script kita sama seperti pada saat kita menjalankan `deno run --allow-read --unstable read_json.ts` ?. Setelah saya cari di google ternyata ada tambahan argumen `-S` pada perintah `env` agar shell script dapat menerima argumen tambahan dari Deno. 

{{< highlight typescript >}}
#!/usr/bin/env -S deno run --allow-read --unstable

import { readJson } from "https://deno.land/std@0.54.0/fs/mod.ts";

const content = await readJson("hello.json");
console.log(content);
{{< / highlight >}}

Sekian, semoga bermanfaat.
