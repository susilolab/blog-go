---
categories: rust
date: "2020-11-13T06:00:00Z"
excerpt_separator: <!--more-->
title: JSON encode dan decode pada Rust
---

Hari ini saya ingin berbagi sedikit ilmu tentang bagaimana encode dan decode json objek di bahasa pemgrograman Rust. Karena saya masih newbie di Rust mohon dikoreksi jika terdapat 
kesalahan pada artikel ini. Ok, langsung saja pertama-tama kita buat project baru dengan nama `demo-json`. Silahkan ketikan perintah berikut untuk membuat project baru
<!--more-->

{{< highlight bash >}}
$ cargo new demo-json --bin
{{< / highlight >}}

Kemudian tambahkan pustaka serde dengan fitur derive dan serde_json pada dependencies

{{< highlight toml >}}
[dependencies]
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
{{< / highlight >}}

Setelah selesai menambah dependencies buka file `main.rs` kemudian import struk Serialize dan Deserialize.

{{< highlight rust >}}
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct Todo {
  id: i32,
  title: String
}

fn main() {
  let obj: Todo = Todo {
    id: 1,
    title: "Belajar Rust".to_string(),
  };

  let enc = serde_json::to_string(&obj).unwrap();
  println!("{}", enc);

  let dec: Todo = serde_json::from_str(&enc).unwrap();
  println!("{:?}", dec);
}
{{< / highlight >}}

Jalankan kode di atas dengan mengetik `cargo run` pada terminal dan terminal akan mencetak string dan struk todo.

```
   Compiling demo-json v0.1.0
    Finished dev [unoptimized + debuginfo] target(s) in 0.74s
     Running `target/debug/demo-json`
{"id":1,"title":"Create web"}
Todo { id: 1, title: "Create web" }
```

Bagaimana cara kerjanya, mari kita telusuri satu persatu. Pertama kali mengimport trait `Serialize` dan `Deserialize` menggunakan keyword `use`.
Trait ini akan digunakan pada struk `Todo` yang akan otomatis mengimplementasi fungsi-fungsi yang ada pada trait `Serialize` dan `Deserialize`.
Sehingga kita tidak perlu secara manual mengetikkan script seperti ini `impl Serialize for Todo {}` dan seperti ini `impl Deserialize for Todo`.

{{< highlight rust >}}
use serde::{Serialize, Deserialize};
{{< / highlight >}}

Kemudian kode berikutnya

{{< highlight rust >}}
#[derive(Serialize, Deserialize, Debug)]
struct Todo {
  id: i32,
  title: String
}
{{< / highlight >}}

Kode di atas akan membuat struk `Todo`, perhatikan syntax `#[derive()]`. syntax ini akan secara otomatis mengimplementasi trait yang ada pada antara 
tanda kurung buka dan tutup (`()`) untuk struk `Todo`. Struk `Todo` berisi 2 field yaitu `id` yang bertipe integer 32 dan `title` yang bertipe string.

Lalu pada fungsi main, kita membuat variabel `obj` yang bertipe `Todo` dan memberikan nilai pada field id dan title.

{{< highlight rust >}}
let obj: Todo = Todo {
  id: 1,
  title: "Belajar Rust".to_string(),
};
{{< / highlight >}}

Kode berikutnya kita membuat 2 variabel yaitu `enc` yang berisi hasil encode struk ke json dan variabel `dec` menyimpan hasil decode dari string ke 
struk `Todo`. Disini penulis menggunakan fungsi `to_string` dan `from_str` dari pustaka `serde_json`.

{{< highlight rust >}}
let enc = serde_json::to_string(&obj).unwrap();
println!("{}", enc);

let dec: Todo = serde_json::from_str(&enc).unwrap();
println!("{:?}", dec);
{{< / highlight >}}

Demikian semoga yang sedikit ini bermanfaat

