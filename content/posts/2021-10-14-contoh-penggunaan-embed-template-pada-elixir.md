---
categories: elixir
date: "2021-10-14T07:00:00Z"
title: Contoh Penggunaan Embed Template pada Ecto
---

Jika ingin membuat generator untuk Ecto versimu sendiri, templatenya bisa menggunakan fungsi dari 
modul `Mix.Generator`.

<!--more-->

{{< highlight elixir >}}
defmodule MyApp do
  import Mix.Generator

  def hello do
    sample_template([log_msg: "Pesan untuk log"]) |> IO.inspect()
  end

  embed_template :sample, """
  Log: <%= @log_msg %>
  """
end

MyApp.hello()
{{< / highlight >}}

`embed_template` akan menghasilkan fungsi private sesuai dengan namanya. Misal parameter nama diisi 
`:sample` maka nama fungsinya akan menjadi `sample_template` dan hanya bisa diakses didalam modul itu sendiri.
