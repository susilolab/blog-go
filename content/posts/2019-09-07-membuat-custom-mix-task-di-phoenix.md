---
categories: elixir
date: "2019-09-07T08:00:00Z"
title: Membuat custom Mix Task di Phoenix
---

Kadang kita butuh membuat task versi kita sendiri untuk mempermudah dalam pengembangan web. Bawaan phoenix mendukung pembuatan custom mix task dan gunakan modul `OptionParser` untuk mengolah argumennya. Untuk membuat custom mix task bikin folder `app_name/lib/mix/tasks` dan buat file baru misalnya `hello.ex` kemudian ikuti kode berikut:
<!--more-->

{{< highlight elixir >}}
defmodule Mix.Tasks.Hello do
    use Mix.Task
    
    def run(_) do
        Mix.Task.run("app.start")
        Mix.shell.info("Hello world!")
    end
end
{{< / highlight >}}

`Mix.Task.run` memastikan bahwa saat mix dijalankan aplikasi juga dijalankan. Setelah itu compile dengan perintah `mix compile` dan jalankan dengan perintah `mix hello`.