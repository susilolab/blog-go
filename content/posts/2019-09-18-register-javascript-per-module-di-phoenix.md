---
categories: elixir
date: "2019-09-18T08:00:00Z"
title: Register Javascript per modul di Phoenix
---

Dalam mengembangkan aplikasi web dengan framework [`Phoenix`](https://phoenixframework.org/) kadang kita butuh menyertakan file javascript hanya pada halaman tertentu saja. Karena di Phoenix semua file javascript dijadikan satu pada file `app.js` yang nantinya akan dikompilasi menggunakan webpack, namun phoenix menyediakan cara untuk merender view hanya jika view tersebut ada yaitu pakai fungsi `render_existing/3`.
<!--more-->

{{< highlight elixir >}}
render_existing(module, template, assigns \\ [])
{{< / highlight >}}

Contoh struktur aplikasi phoenix

![struktur-aplikasi](/img/struktur-app-2019-09-18-110906.png)

Dalam kasus ini kita akan membuat file `book.js` hanya dimuat pada controller Buku saja, untuk pertama-tama kita salin/buat file `book.js` didalam folder `priv/static/js` kemudian pada file layout `lib/book_store_web/templates/layout/app.html.eex` tambahkan kode berikut

{{< highlight elixir >}}
<%= render_existing(@view_module, "script.html", assigns) %>
{{< / highlight >}}

Dibawah kode ini:

{{< highlight elixir >}}
<script type="text/javascript" src="<%= Routes.static_path(@conn, "/js/app.js") %>"></script>
{{< / highlight >}}

Lalu bikin fungsi render pada file `lib/book_store_web/views/book_view.ex`

{{< highlight elixir >}}
def render("script.html", _assigns) do
  ~s{<script type="text/javascript" src="/js/book.js"></script>}
  |> raw
end
{{< / highlight >}}

semoga bermanfaat
