---
categories: php
date: "2021-09-05T07:00:00Z"
title: Destructuring Array pada PHP
---

Assalamu'alaikum, Hari ini saya mendapatkan pelajaran baru pada saat mengejarkan pekerjaan dikantor tentang destructuring array.
Destructuring lawan dari structuring itu artinya dari yang sudah punya struktur kita bongkar menjadi tidak punya struktur.
Manfaatnya ya akan mempermudah dalam mengambil data pada array. Coba kita lihat pada contoh berikut
<!--more-->

Oke misalnya kita punya data array angka 1,2 dan 3 dan ingin mengekstrak data tersebut. sebelum ada destructuring kita akan melakukannya 
seperti ini
{{< highlight php >}}
$angka = [1, 2, 3];
echo $angka[0];
echo $angka[1];
echo $angka[2];
{{< / highlight >}}

Maka kita akan mendapat nilai 1,2 dan 3. Dengan destructuring array hal ini akan menjadi lebih ringkas
```
$angka = [1, 2, 3];
[$a, $b, $c] = $angka;
echo $a;
echo $b;
echo $c;
```

Disini kita menggunakan destructuring berdasarkan index pada array, bagi yang pernah belajar pemrograman fungsional hal ini disebut 
pencocokan pola(pattern matching).
Oke kita bisa destructuring array menggunakan indexnya, bagaimana kalau destructuring array associate?

Kita juga bisa destructuring array dengan index string, caranya seperti ini:
```
$person = ['name' => 'Agus', 'age' => 35];
['name' => $name, 'age' => $age] = $person;
echo "$name, $age";
```

**Catatan:** Sintak baru `[]` hanya ada pada PHP versi 7.1 atau yang lebih baru. Untuk versi di bawahnya 
bisa menggunakan fungsi `list`.
{: .note-tip }

Semoga bermanfaat, sekian.
