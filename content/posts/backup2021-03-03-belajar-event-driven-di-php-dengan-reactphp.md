---
categories: php
date: "2021-03-03T07:00:00Z"
title: Belajar Event Driven di PHP dengan ReactPHP
---

Tahukan kamu bahwa `PHP` dapat melakukan sesuatu secara asynchronous ?. Saya baru mencoba menggunakan 
library [`react/mysql`](https://github.com/friends-of-reactphp/mysql) untuk query data dengan men-select 2 field dengan limit 10.000 dan hasilnya luar biasa.
Ndak sampai 1 detik untuk mencetak 10.000 data tsb.
<!--more-->

Bagi yang belum tahu apa itu [`ReactPHP`](https://reactphp.org/), [`ReactPHP`](https://reactphp.org/) adalah low-level library untuk event-driven programming di PHP. Intinya terdiri dari event loop pada level atas yang menyediakan utility low-level seperti Streams abstraction, async DNS resolver, network client/server, HTTP client/server dan interaksi dengan proses.

ReactPHP mampu melakukan proses secara asynchronous

{{< highlight php >}}
<?php
require(dirname(__FILE__) . '/vendor/autoload.php');

use React\MySQL\Factory;
use React\MySQL\QueryResult;

$loop = React\EventLoop\Factory::create();
$factory = new Factory($loop);
$uri = 'user:passwd@127.0.0.1/test';

$conn = $factory->createLazyConnection($uri);
$conn->query('select user_id, username FROM user LIMIT 10000')->then(
    function (QueryResult $command) {
        print_r($command->resultRows);
        echo count($command->resultRows) . ' row(s) in set' . PHP_EOL;
    },
    function (Exception $e) {
        echo 'Error: ' . $error->getMessage() . PHP_EOL;
    }
);
$conn->quit();

$loop->run();
{{< / highlight >}}

Sekian
