---
categories: ruby
date: "2020-05-21T08:00:00Z"
title: Link Internal Post pada Artikel di Jekyll
---

Saya ingin berbagi tips singkat bagaimana membuat link yang merujuk ke `internal post` di jekyll. Contoh berikut ini untuk postingan bertipe markdown.
<!--more-->

{{< highlight ruby >}}
[Judul Post]({% post_url 2020-05-15-judul-post %})
{{< / highlight >}}

Perlu diperhatikan bahwa nama artikel setelah variabel `post_url` harus benar dan artikelnya ada. Dan semua kata dipisah dengan tanda dash(-).

Sekian, semoga bermanfaat.
