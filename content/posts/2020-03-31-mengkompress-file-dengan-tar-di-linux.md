---
categories: linux
date: "2020-03-31T08:00:00Z"
title: Mengkompress file dengan tar di linux
---

Bismillah,
Pernahkah temen-temen semua ingin mengkompress file di server linux tapi ndak tahu perintahnya apa?. Nah disini saya ingin berbagi tips bagaimana caranya mengkompres file/folder kedalam format tertentu dengan perintah `tar`, berikut ini cara mengkompres file/folder dengan format mulai dari `.tar`, `tar.bz2`, `tar.gz` dan `tar.xz`.
<!--more-->

#### tar.bz2
{{< highlight bash >}}
$ tar cjf nama_file.tar.bz2 nama_folder/
{{< / highlight >}}

#### ekstrak file tar.bz2
{{< highlight bash >}}
$ tar -jxvf nama_file.tar.bz2
{{< / highlight >}}

#### tar.gz
{{< highlight bash >}}
$ tar czf nama_file.tar.gz nama_folder/
{{< / highlight >}}

#### ekstrak file tar.gz
{{< highlight bash >}}
tar -zxvf nama_file.tar.gz
{{< / highlight >}}

#### .tar
{{< highlight bash >}}
$ tar cf nama_file.tar nama_folder/
{{< / highlight >}}

#### ekstrak file .tar
{{< highlight bash >}}
$ tar -xvf nama_file.tar
{{< / highlight >}}

#### tar.xz
{{< highlight bash >}}
$ tar cfJ nama_file.tar.xz folders/
$ tar -cvJf nama_file.tar.xz folders/
{{< / highlight >}}

#### ekstrak file tar.xz
{{< highlight bash >}}
$ tar -xJf p.tar.xz
{{< / highlight >}}

Dari ke-4 format di atas yang paling kecil rasio kompresnya yaitu `tar.xz` tetapi tidak didukung oleh windows. Jika kamu memilih kecepatan namun tidak terlalu memikirkan masalah ukuran file maka `tar.gz` paling pas karena selain cepat juga hasil kompresnya lumayan kecil.


#### Tips menghafal perintah di atas
Coba perhatikan perintah di atas semuanya menggunakan parameter `cf` tinggal kita menghafalkan huruf selain `c` dan `f` yaitu:

| j  | Untuk format .tar.bz2           |
| z  | Untuk format .tar.gz            |
| J  | Untuk format .tar.xz            |
| _  | Cuma c dan f, untuk format .tar | 


Semoga bermanfaat, sekian.