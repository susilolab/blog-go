---
categories: rust
date: "2020-05-15T03:00:00Z"
title: Mengenal Tipe Data Dasar pada Rust
---

Rust memiliki beberapa tipe data dasar antara lain

* booleans - `bool` untuk data berupa benar/salah true dan false
* integer tak bertanda - `u8`, `u32`, `u64`, `u128` untuk mewakili bilangan bulat positif
* integer bertanda - `i8`, `i32`, `i64`, `i128` untuk mewakili bilangan bulat positif dan negatif
* pointer sized integer - `usize`, `isize` untuk mewakili data yang berindex dan memiliki ukuran dimemori
* bilangan pecah - `f32`, `f64`
* terkait text - `str`, `chr`
* tuple - `(value, value, ...)` untuk data berurutan dengan ukuran tetap
* slices - `&[T]` untuk mewakili data berurutan pada memori atau biasa dikenal dengan nama array/larik
<!--more-->

Untuk tipe data text mungkin akan lebih komplek dari pada yang biasa kamu gunakan pada bahasa pemrogramana lain dan tidak akan dibahas disini.

Catatan bahwa tipe data numerik dapat ditulis tipenya secara eksplisit dengan menambahkan tipe setelah angka. (contoh: 12u32)

Contoh kodenya

{{< highlight rust >}}
let angka = 10;
let nama = "Agus";
{{< / highlight >}}

Untuk membuat variabel Rust memiliki keyword `let` yang berguna untuk mendefinisikan variabel dan default variabelnya immutable atau tidak dapat dirubah.

Rust memiliki fitur Type Inference yang sangat berguna bagi pengembang, jadi kita boleh membuat variabel tanpa menyebutkan tipe datanya seperti pada contoh di atas.

Contoh kode lengkap:

{{< highlight rust >}}
let angka = 10; // defaultnya i32
let nama = "Agus"; // defaultnya &str
let angka_lain = 20u32;
let memsize: usize = 1usize;
let jutaan = 1_000_000;
let bil_pecah: f64 = 1.00;
let hello: String = String::from("Hello world!");
let tuple = (1, 2, 3);
let slices = &[1, 2, 3];

println!("{} {} {} {} {} {} {} {:?} {:?}",
    angka,
    nama,
    angka_lain,
    memsize,
    jutaan,
    bil_pecah,
    hello,
    tuple,
    slices
);
{{< / highlight >}}

Pada variabel `jutaan` di atas Rust juga membolehkan menulis angka dengan menambahkan underscore agar angka lebih mudah terbaca.

Selain tipe data yang saya sebutkan di atas Rust masih memiliki tipe data lain seperti `String`, `Vector`, `HashMap`. Insya Allah pada kesempatan berikutnya saya akan membahasa tentang konversi dari tipe data satu ke tipe data lain dan juga tentang `&str` dan `String`

Semoga pengenalan tipe data ini dapat bermanfaat. sekian.