---
categories: rust
date: "2020-09-23T07:00:00Z"
title: Perulangan terbalik pada rust
---

Kali ini saya ingin berbagi sedikit tip bagaimana melakukan loop secara terbalik pada Rust.
Biasanya kita akan melakukan loop seperti ini:
<!--more-->

{{< highlight rust >}}
...
for i in 0..5 {
    println!("{}", i);
}
...
{{< / highlight >}}

Bagaimana jika ingin loop dari 5 s/d 0 ?. Mungkin kita akan berpikiran seperti ini

{{< highlight rust >}}
...
for i in 5..0 {
    println!("{}", i);
}
...
{{< / highlight >}}

Jika dijalankan kode di atas tidak akan jalan. Lalu bagaimana cara yang benar jika ingin perulangannya terbalik ?. 
Kita bisa menggunakan unit jangkuan dan fungsi `rev()`.

{{< highlight rust >}}
...
for i in (0..5).rev() {
    println!("{}", i);
}
...
{{< / highlight >}}

Kode di atas akan jalan dengan sukses dan menghasilkan keluaran seperti berikut ini:

{{< highlight bash >}}
4
3
2
1
0
{{< / highlight >}}

Demikian tips singkat kali ini semoga bermanfaat.
