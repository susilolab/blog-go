---
categories: php
date: "2022-02-15T06:03:00Z"
title: Mendapatkan Raw Sql dari ActiveRecord Yii2
---

Tips singkat cara mendapatkan `raw sql` dari ActiveRecord pada Yii2. Cukup hilangkan fungsi chain `one`/`all`/`scalar`/dll
dan tambahkan `createCommand()->getRawSql()`.
<!--more-->

{{< highlight php >}}
$user = User::find()->where(['id' => 1]);
echo $user->createCommand()->getRawSql(), PHP_EOL;
{{< / highlight >}}
