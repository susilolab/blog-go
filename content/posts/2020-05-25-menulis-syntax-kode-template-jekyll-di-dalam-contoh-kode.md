---
categories: ruby
date: "2020-05-25T05:00:00Z"
title: Menulis Syntax Kode Template Jekyll di dalam Contoh Kode
---

Buat temen-temen yang pake jekyll untuk menulis blognya pasti tahu syntax dari template jekyll. Nah pernahkah temen-temen ingin men-share kode dari template jekyll?. Sudah mencari-cari digoogle tapi belum ketemu?. Kali saya ingin berbagi bagaimana men-share kode dari syntax template jekyll ke blog.
<!--more-->

Untuk bisa menuliskan kode dari jekyll kita bisa memakai syntax `raw` dan `endraw`, karena saya belum menemukan bagaimana men-share kode `raw` di dalam `raw` maka saya pake screenshot saja. Jadi setelah syntax kode `highlight` kita bisa tambahkan `raw` kemudian baru menulis kode template. `raw` akan memproses apa yang ada di dalamnya sesuai format, tanpa merubah isinya. 

![mix-deps-get](/img/20200522-syntax-jekyll.png)

Contoh outputnya bisa dilihat pada postingan saya berikut [Link Internal Post pada Artikel di Jekyll]({% post_url 2020-05-21-link-internal-post-pada-artikel-di-jekyll %})

Sekian semoga bermanfaat.
