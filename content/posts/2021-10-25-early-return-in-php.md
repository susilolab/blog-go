---
categories: php
date: "2021-10-25T07:00:00Z"
title: Early return in PHP
tags:
- php
---

Sebagai seorang programmer tentu selalu ingin meningkatkan kualitas kodenya agar ringkas dan mudah dipahami. Nah kali ini saya ingin berbagi sedikit tip 
tentang early return di PHP, apa itu early return?. Sesuai dengan namanya `mengembalikan lebih awal` yaitu mengembalikan nilai dari suatu fungsi lebih 
awal. Keuntungan mengembalikan nilai lebih awal akan membuat kode lebih ringkas dan dapat menghindari if bersarang. Coba bandingkan kedua fungsi di bawah ini
<!--more-->

{{< highlight php >}}
// Fungsi 1
function isUserSubcribe(int $userId): bool
{
    $model = ContentSubscribe::find()
        ->where(['user_id' => $userId])->one();
    if ($model != null) {
        return true;
    } else {
        return false;
    }
}

// Fungsi 2
function isUserSubcribe(int $userId): bool
{
    $model = ContentSubscribe::find()
        ->where(['user_id' => $userId])->one();
    if ($model == null) {
        return false;
    }

    return true;
}
{{< / highlight >}}

Terlihat bahwa fungsi no 2 lebih ringkas dengan penggunaan early return. Early return juga akan sangat membantu pada kode yang banyak menggunakan 
if bersarang.

{{< highlight php >}}
function checkAccess(int $userId, string $page): bool
{
    $model = AuthItem::find()->where(['user_id' => $userId])->one();
    if ($model != null) {
        if ($model->page == $page) {
            return true;
        }
    }

    return false;
}
{{< / highlight >}}

Kode di atas bisa diringkas lagi menggunakan early return.

{{< highlight php >}}
function checkAccess(int $userId, string $page): bool
{
    $model = AuthItem::find()->where(['user_id' => $userId])->one();
    if ($model == null) {
        return false;
    }
    
    if ($model->page == $page) {
        return true;
    }

    return false;
}

// Atau logiknya bisa disederhanakan
function checkAccess(int $userId, string $page): bool
{
    $model = AuthItem::find()->where(['user_id' => $userId])->one();
    if ($model == null || ($model != null && $model->page != $page)) {
        return false;
    }
    
    return true;
}
{{< / highlight >}}

Cukup sekian, semoga dapat memberikan manfaat bagi teman-teman.
