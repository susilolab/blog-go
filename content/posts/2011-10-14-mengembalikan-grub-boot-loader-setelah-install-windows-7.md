---
categories: linux
date: "2011-10-14T08:00:00Z"
title: Mengembalikan grub boot loader setelah install Windows 7
---

Cara ini sudah saya coba beberapa kali di laptop dengan sistem operasi Linux Mint 11 dan Windows 7. Kasusnya begini setelah install ulang linux Mint 11 kemudian saya install windows 7 dan otomatis grub boot loader akan tertumpuk dengan bootnya windows.
<!--more-->

Untuk itu booting dengan linux live cd misalnya Mint 11 kemudian buka terminal dan ketik perintah ini:

{{< highlight bash >}}
$ sudo mount /dev/sda1 /boot
$ sudo grub-install /dev/sda
{{< / highlight >}}

Dengan asumsi bahwa partisinya seperti berikut ini:

{{< highlight bash >}}
/dev/sda1 –> Untuk menyimpan dual booting (Primary)
/dev/sda2 –> Partisi windows C (Primary)
/dev/sda3 –> Partisi windows D (Logical/Extended)
/dev/sda4 –> Partisi Linux (Logical/Extended)
/dev/sda5 –> Swap (Logical/Extended)
{{< / highlight >}}

Semoga bermanfaat 🙂
