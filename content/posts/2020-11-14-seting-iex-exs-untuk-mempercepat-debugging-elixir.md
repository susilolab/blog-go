---
categories: elixir
date: "2020-11-14T06:00:00Z"
title: Seting .iex.exs untuk mempercepat debugging Elixir
---

Bagi programmer Elixir tentu sudah tidak asing lagi dengan aplikasi `iex` atau interactive Elixir, aplikasi ini biasanya digunakan untuk tes aplikasi atau sekedar mencoba kode 
(fungsi) Elixir sebelum ditulis ke file dll. Biasanya kita menjalankan `iex` pada mix project dengan perintah 
`iex -S mix` maka otomatis modul2 pada project dapat kita akses. Saat kita ingin mencoba query kita harus mengimport dulu modul2 yang bersangkutan dan juga membuat aliasnya.
<!--more-->

Contoh

{{< highlight elixir >}}
iex(1)> import Ecto.Query, only: [from: 2]
iex(2)> alias SipApp.Repo
iex(3)> alias SipApp.Sip.Book
iex(4)> # Lalu memulai tes query
iex(5)> Repo.one(Book)
{{< / highlight >}}

Langkah-langkah di atas selalu kita ulangi berkali-kali setiap masuk ke interaktif Elixir. Tahukah kamu bahwa langkah tersebut dapat kita masukan ke dalam file config
dengan nama `.iex.exs` dan memasukan langkah-langkah di atas kedalam script, sehingga pada saat memulai `iex` kita tidak perlu lagi menuliskan import dan membuat alias.
Caranya buat file dengan nama `.iex.exs` dan letakkan di dalam proyek, misalkan `sip_app/.iex.exs`.

Contoh

{{< highlight elixir >}}
import Ecto.Query, only: [from: 2]
alias SipApp.Repo
alias SipApp.Sip.Book
{{< / highlight >}}

Kemudian kita bisa mencobanya dengan menjalankan `iex -S mix` dan mencoba langsung query misalnya `Repo.one(Book)`. Jika tidak ada eror maka confignya berhasil.

Sekian, semoga bermanfaat
