---
categories: elixir
date: "2019-09-09T08:00:00Z"
title: Menghitung total array angka dengan fungsi reduce
---

Kali ini saya ingin berbagi tips bagaimana menjumlahkan total array angka di Elixir dengan fungsi `Enum.reduce/3`. Bagi yang pernah mengenal bahasa pemrograman seperti C, C++, Java atau PHP tentunya hal ini akan sangat-sangat mudah, saya beri contoh kodenya dalam bahasa PHP.
<!--more-->

{{< highlight php >}}
<?php
$angka = [1,2,3,4,5];
$total = 0;
foreach($angka as $val) {
    $total += $val;
}

echo $total;
{{< / highlight >}}

Bisa kita lihat bahwa untuk menghitung total angka kita bikin variabel `total` kemudian looping `angka` sampai habis dan hasilnya di tambahkan ke variabel `total`. Beda dengan elixir karena bahasa elixir termasuk dalam kategori functional programming maka cara yang ditempuh berbeda. Lihat kode berikut ini:

{{< highlight elixir >}}
angka = [1,2,3,4,5]
total = Enum.reduce(angka, 0, fn(x, acc) -> acc + x end)
IO.puts "#{total}"
{{< / highlight >}}

Bisa dilihat bahwa elixir menggunakan fungsi `reduce` untuk menghitung total angka karena pada elixir setiap variabel bersifat immutable/tidak bisa dirubah dan elixir merupakan functional programming. Untuk penjelasan apa itu **Pemrograman Fungsional** Insya Allah saya jelaskan pada kesempatan berikutnya. sekian.