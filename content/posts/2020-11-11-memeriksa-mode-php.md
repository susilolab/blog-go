---
categories: php
date: "2020-11-11T06:00:00Z"
title: Memeriksa mode PHP
tags:
- php
---

Kali ini saya ingin berbagi sedikit tip bagaimana cara mengetahui apakah php sedang berjalan di `commandline` atau web. Kita dapat menggunakan fungsi
`php_sapi_name`, fungsi ini akan mengembalikan string mode yang sedang berjalan. Nilai kembaliannya bisa berupa `cli`, `cli-server`, `cgi-fcgi`, `embed`, `fpm-fcgi`,
`litespeed`, `nsapi` dan `phpdbg`.
<!--more-->

Coba salin kode berikut dan jalankan dengan mengetikan pada terminal `php cli.php`

{{< highlight php >}}
<?php
echo php_sapi_name(), "\n";
{{< / highlight >}}

Maka akan menghasilkan output `cli`. Jika php dijalankan dari nginx dengan mode fpm maka akan menghasilkan output `fpm-fcgi`.
Fungsi `php_sapi_name` ini dapat berguna pada saat kita mengembangkan aplikasi berbasis web dan menambahkan sistem queue yang berbasis cli. Karena pada saat 
php berjalan di command line beberapa fungsi tidak dapat berjalan seperti `session`.

Sekian, semoga bermanfaat
