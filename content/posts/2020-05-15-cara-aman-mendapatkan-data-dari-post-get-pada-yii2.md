---
categories: php
date: "2020-05-15T04:00:00Z"
title: Cara Aman Mendapatkan data dari POST/GET pada Yii2
---

Yii2 menyertakan fungsi-fungsi yang biasa sering kita gunakan secara lengkap dan aman, biasanya kita sering menggunakan fungsi `$_GET['nama_var']` dan `$_POST['nama_var']` untuk mendapatkan data dari user yang dikirim melalui post/get. namun cara tersebut kurang aman karena tidak adanya nilai default jika variabelnya kosong.
<!--more-->

Contoh:

{{< highlight php >}}
$id = $_GET['id'];
$query = "SELECT * FROM user WHERE id = $id";
{{< / highlight >}}

Agar variabel `$id` lebih aman maka gunakan fungsi `get` atau `post` yang terdapat pada class `yii\web\Request` yang secara global terdapat pada `Yii::$app->request`.

{{< highlight php >}}
$id = Yii::$app->request->get('id', 0);
// Atau bisa ditambahkan kondisi untuk memeriksa variabel id
// if ((int)$id < 1) {
//     throw new \yii\web\HttpException(403, 'ID tidak benar');
// }
$query = "SELECT * FROM user WHERE id = $id";
{{< / highlight >}}

Untuk query-nya agar lebih aman biasakan pakai punyanya Yii2 yaitu class `yii\db\Command` atau `yii\db\Query` karena minim kemungkinan terkena SQLInjection.

{{< highlight php >}}
$id = Yii::$app->request->get('id', 0);
$result = Yii::$app->db
    ->createCommand('SELECT * FROM user WHERE id = :id', [':id' => $id])
    ->queryOne();
{{< / highlight >}}

Untuk variabel `$_POST` juga sama dengan get, tinggal ganti fungsi get dengan post.

{{< highlight php >}}
$id = Yii::$app->request->post('id', 0);
{{< / highlight >}}

Demikian semoga dapat bermanfaat dan terima kasih sudah mampir ke blog saya ini.

Sekian.