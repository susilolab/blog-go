---
categories: php
date: "2021-03-06T07:00:00Z"
title: Mengenal array_filter, array_map dan array_reduce pada PHP
---

Assalamu'alaikum, pada kesempatan kali ini saya ingin mengenalkan penggunaan fungsi `array_filter`,
`array_map` dan `array_reduce` untuk mengolah data array. Biasanya kita mengolah data array dengan cara lama 
yaitu dengan fungsi `for` atau `foreach` digabung dengan `if` untuk kondisinya. Nah kali ini saya akan 
menggunakan fungsi `array_[filter|map|reduce]`. Kita akan lihat kelebihan menggunakan cara ini.
<!--more-->

## array_filter

Pertama saya akan mendemokan dengan fungsi `array_filter` dulu, fungsi ini berguna untuk menyaring data array.
Misalnya kita punya data user yang berisi id, name dan group_id dan kita ingin menyaringnya berdasarkan idnya.
Kita lihat dulu cara lamanya
{{< highlight php >}}
class User
{
    public $id;
    public $name;
    public $group_id;

    public function __construct($id, $name, $group_id = 2)
    {
        $this->id = $id;
        $this->name = $name;
        $this->group_id = $group_id;
    }
}

$users = [
    new User(1, 'admin', 1),
    new User(2, 'editor'),
    new User(3, 'author')
];

$admins = [];
foreach ($users as $user) {
    if ($user->group_id == 1) {
        $admins[] = $user;
    }
}
{{< / highlight >}}

Dengan menggunakan fungsi `array_filter` kita bisa mengurangi side efek samping dari fungsi.
{{< highlight php >}}
$admins = array_filter($data, fn($user) => $user->group_id == 1);
{{< / highlight >}}

Contoh lain mencari deret angka genap
{{< highlight php >}}
$numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$even = array_filter($numbers, fn($num) => $num % 2 == 0);
{{< / highlight >}}

## array_map

`array_map` berguna untuk menerapkan fungsi ke sebuah data array. Misalnya kita punya data array angka 
dan kita ingin mengkalikan tiap data tersebut dengan 2.

{{< highlight php >}}
$numbers = [1, 2, 3, 4];
$multiple = array_map(fn($x) => $x * 2, $numbers);
{{< / highlight >}}

Contoh lain misalnya kita punya data berupa daftar nama dan ingin dirapikan huruf besar kecil.
{{< highlight php >}}
$names = ['agus', 'BuDI', 'nurHayati', 'Adi wijaya'];
$filteredNames = array_map(function ($name) {
    $lowerName = strtolower($name);
    return ucwords($lowerName);
}, $names);
{{< / highlight >}}

## array_reduce

Array reduce tidak hanya merubah jumlah elemen tetapi juga merubah data

{{< highlight php >}}
$numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$odd = array_reduce($numbers, function ($acc, $num) {
    if ($num % 2 == 1) {
        $acc[] = $num * 2;
    }
    return $acc;
}, []);
{{< / highlight >}}

Kelebihan dari penggunaan fungsi array_filter, array_map dan array_reduce adalah kita menerapkan salah satu prinsip pemrograman fungsional yaitu 
tidak merubah data asli. Tidak merubah data asli berarti tidak menimbulkan efek samping. 
Untuk lebih memahami apa efek samping mari kita lihat fungsi sederhana berikut ini:

{{< highlight php >}}
function add($x) {
    return $x + $number;
}
{{< / highlight >}}


Fungsi add di atas memiliki efek samping karena variabel `$number` berada diluar fungsi ataupun tidak ada diparameter fungsi yang mengakibatkan 
nilai yang tidak tentu jika menerapkan tes terhadap fungsi tersebut. Agar fungsi add tidak memiliki efek samping maka kita harus menaruh variabel 
`$number` sebagai parameter fungsi.

{{< highlight php >}}
function add($x, $number) {
    return $x + $number;
}
{{< / highlight >}}

dengan menaruh parameter `$number` sebagai argumen fungsi maka fungsi add akan selalu menghasilkan nilai yang sama jika kita mentesnya. misal kita 
berikan nilai 1 pada `$x` dan 2 pada `$number` maka hasilnya akan selalu 3 karena fungsi tersebut tidak memiliki side effect atau lebih dikenal dengan istilah 
`pure function`. Walaupun PHP sendiri bukan merupakan bahasa pemrograman fungsional.
