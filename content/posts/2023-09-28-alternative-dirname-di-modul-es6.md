---
categories: javascript
date: "2023-09-28T05:00:00Z"
title: Alternative __dirname di module es6
---

Jika kamu seorang programmer Javascript dan sudah mulai menggunakan modul es6 maka kamu perlu tips singkat ini untuk dapat menggunakan variable `__dirname` di scriptmu.
Karena pada modul es6 variable `__dirname` tidak dapat diakses.

<!--more-->

Bagaimana mengetahui bahwa aplikasi kita sudah menggunakan modul es6?. hal ini bisa dilihat dari cara import sebuah modul, sebelum modul es6 cara import modul menggunakan syntax
`require`. Contoh:

{{< highlight javascript >}}
const app = require('express')
{{< / highlight >}}

sedangkan pada modul es6 sudah menggunakan syntax `import`

{{< highlight javascript >}}
import app from 'express'
{{< / highlight >}}

Hal ini ditandai dengan adanya properti konfig dengan nama `type` dan bernilai `module` pada file `package.json`. Nah jika sudah menggunakan modul es6 maka variable `__dirname` tidak dapat dipakai lagi.
Untuk itu berikut ini ada caranya membuat semisal `__dirname`, kamu cukup salin saja kode berikut dan variable `__dirname` sudah bisa digunakan.

{{< highlight javascript >}}
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'

const __dirname = dirname(fileURLToPath(import.meta.url))
{{< / highlight >}}

semoga bermanfaat.
