---
categories: linux
date: "2021-10-13T06:00:00Z"
title: Mengekstrak file dari mercurial hg
---

Sebagai seorang software engineer tentu tak lepas dari git/hg (mercurial) dan tentu saja tak lepas dari kesalahan.
Pernah mem-merge 2 perubahan kode yang berbeda ? dan tak sengaja menghapus perubahan rekan setim ?. Kalau Anda pernah mengalaminya
berarti kita sama, saya juga pernah mengalaminya. 
<!--more-->

Tidak perlu khawatir file-filemu masih bisa diambil selama sudah di commit. Cara ini akan mengambil file dari sebuah repositori di `hg` berdasarkan nomor revisi. 
Tetapi cara ini hanya bisa dilakukan dengan command line. Caranya seperti berikut:

`$ hg cat -r no_revisi lokasi_file > lokasi_tujuan`

Contoh:

{{< highlight bash >}}
$ hg cat -r 2562 protected/models/User.php > /home/user/User.php
{{< / highlight >}}

Perintah di atas akan menyimpan file `User.php` dari revisi no 2562 ke folder `/home/user` dengan nama `User.php`.

Demikian tips singkat ini semoga bermanfaat.
