---
author: Agus Susilo
categories: php
date: "2021-10-21T07:00:00Z"
title: Passing argument by reference in PHP
---

Bagi programmer PHP tentu tidak asing lagi dengan namanya argumen pada fungsi, namun tahukah teman-teman bahwa argumen fungsi pada PHP defaultnya 
dilewatkan berdasar nilai. Jadi variabel yang dilewatkan pada fungsi akan disalin kedalam fungsi. Mari kita lihat contoh berikut
<!--more-->

{{< highlight php >}}
function inc(int $num): int
{
    $num++;
}

$i = 1;
inc($i);
inc($i);

echo $i . PHP_EOL;
{{< / highlight >}}

Bisa tebak nilai yang ada didalam variabel `$i`?, apakah angka 2 karena fungsi `inc` dijalankan 2 kali? atau angka 1?.
Jawabannya karena nilai yang dilewatkan ke fungsi `inc` oleh nilai maka setiap kali `inc` dipanggil nilai `$i` akan di naikan 1,
namun peningkatan nilai tersebut hanya pada lingkup fungsi, diluar fungsi nilai `$i` akan tetap 1.

Agar nilai `$i` tetap bertambah setelah fungsi `inc` dipanggil, variabel yang dilewatkan harus dengan referensi. `&` dapat digunakan 
untuk melewatkan argument dengan referensi.

{{< highlight php >}}
function inc(int &$num): int
{
    $num++;
}

$i = 1;
inc($i);
inc($i);

echo $i . PHP_EOL;
{{< / highlight >}}

Setelah argument fungsi ditambah referensi maka nilai yang dilewatkan pada fungsi akan merubah juga ada variabel 
diluar fungsi. Oleh karena hasil dari perubahan ini nilai `$i` adalah 3. Contoh push data ke array

{{< highlight php >}}
function add_lang(array &$langs, string $lang_name) {
    $langs[] = $lang_name;
}

$langs = [];
add_langs($langs, 'PHP');
add_langs($langs, 'Rust');

print_r($langs); // Output: Array([0] => PHP, [1] => Rust)
{{< / highlight >}}

Contoh lain penggunaan pass by reference di Yii 2. Jadi kita akan memecah kondisi query ke dalam fungsi sehingga query yang komplek dapat lebih ringkas dan tidak terkumpul dalam satu fungsi saja.

{{< highlight php >}}
class Demo
{
    public function wherePublic(Query &$query)
    {
        $query->andWhere(['public' => 1]);
    }

    public function actionIndex()
    {
        $query = (new Query())->select('*')->from('user');
        $this->wherePublic($query);

        print_r($query->createCommand()->getRawSql());
        // output: SELECT * FROM `demo_user` `u` WHERE `publish`=1
    }
}
{{< / highlight >}}

Pada fungsi `wherePublic` argumentnya dilewatkan berdasarkan reference dengan ditandai dengan `&` sehingga variabel `$query` akan dimodif didalam fungsi. 
Kemudian pada saat memanggil fungsinya tidak perlu menggunakan tanda `&`, tanda `&` cukup pada definisi fungsi saja.

Sekian semoga dapat memberi manfaat
