---
categories: rust
date: "2020-04-18T08:00:00Z"
title: Cargo run multi binary
---

Pada saat kita menjalankan `cargo run` maka secara default `cargo` akan mencari file dengan nama `main.rs`, bagaimana jika kita ingin menjalankan aplikasi yang berbeda nama file, misal kita ingin agar cargo dapat menjalankan file `publisher.rs` atau `subscriber.rs` pada saat kita ketik perintah `cargo run`. Maka kita bisa menggunakan parameter `--bin nama_binary`:
<!--more-->

{{< highlight bash >}}
$ cargo run --bin publisher
$ cargo run --bin subscriber
{{< / highlight >}}

Agar cargo mengenali pada saat kita menambahkan parameter `--bin nama_binary` maka perlu ditambahkan setingan pada file `Cargo.toml`.

{{< highlight toml >}}
[package]
name = "rabbit"
version = "0.1.0"
authors = ["Name <name@example.com>"]
edition = "2018"

[[bin]]
name = "publisher"
path = "src/publisher.rs"

[[bin]]
name = "subscriber"
path = "src/subscriber.rs"
{{< / highlight >}}

Dengan begini cargo akan mengetahui binary mana yang harus dijalankan selain pada `main.rs`. Kita dapat mendefinisikan nama binary sebanyak mungkin.

Sekian, semoga bermanfaat.
