---
categories: elixir
date: "2019-09-01T15:00:00Z"
title: Baca file di Elixir dengan with
---

Kita tahu elixir kebanyakan menggunakan kembalian berupa tuple `{:ok, hasil}` jika sukses dan tuple `{:error, alasan}` jika error pada fungsinya. Contoh kita akan membaca file dengan `File.read` **tanpa** `with` dan yang kedua **dengan** `with`. Buat file `file_read_with.exs`
<!--more-->

{{< highlight elixir >}}
{:ok, content} = File.read("hello.txt")
IO.puts content
{{< / highlight >}}

Kode diatas jika file yang dibaca tidak ada maka akan error, untuk itu kita gunakan `with` untuk menangani jika terjadi kondisi error.

{{< highlight elixir >}}
with {:ok, item} <- File.read("hello.txt") do
  IO.puts item
else {:error, reason} ->
  IO.puts "Error read file: #{reason}"
end
{{< / highlight >}}

sekian.
