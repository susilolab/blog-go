---
categories: linux
date: "2020-04-14T08:00:00Z"
title: Hg pull dan push dengan switch config antara default dan remote
---

Ingin switch konfigurasi saat menjalankan hg pull dan push?. Misalkan kita punya 2 konfigurasi `default` dan `remote` maka kita dapat menjalankan hg pull dan push seperi berikut ini
<!--more-->

{{< highlight bash >}}
$ hg pull remote --debug
$ hg push default --debug
{{< / highlight >}}

Letak konfig tsb ada di file hgrc

{{< highlight bash >}}
[paths]
default = ssh://username@192.168.1.100//home/username
remote = ssh://username@example.com:5022//home/username
{{< / highlight >}}