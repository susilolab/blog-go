---
categories: elixir
date: "2020-11-10T07:00:00Z"
tags: elixir
title: Mengenal fungsi filter, map dan reduce di Elixir
---

Elixir adalah bahasa pemrograman fungsional, pada saat mengembangkan aplikasi tidak lepas dengan ketiga fungsi tersebut. Nah kali ini saya
ingin berbagi sedikit ilmu tentang 3 fungsi yaitu `filter`, `map` dan `reduce`, apa perbedaannya dan kapan harus menggunakannya.
<!--more-->

**`filter(enumerable, fun)`**

Fungsi `filter` berguna untuk menyaring data sesuai dengan parameter `fun` dan mengembalikan hanya nilai yang benar(sesuai argumen `fun`).
Perlu diperhatikan bahwa fungsi `filter` tidak merubah **tipe data** tetapi hanya merubah **jumlah data**. Misalkan kita ingin mencari semua angka
genap dari sebuah list(array), maka kodenya seperti ini:

{{< highlight elixir >}}
Enum.filter([1, 2, 3, 4, 5], fn x -> rem(x, 2) == 0 end) |> IO.inspect()
{{< / highlight >}}

Bagaimana kalau bentuk datanya berupa list map atau struct?. Penggunaannya akan tetap sama seperti pada tipe data list.
Misal kita akan mencari data dari sebuah list map `User` yang `group_id` nya sama dengan 2.

{{< highlight elixir >}}
user = [
    %{id: 1, group_id: 1, name: "Agus"},
    %{id: 2, group_id: 2, name: "Susilo"}
]
Enum.filter(user, fn x -> x.group_id == 2 end) |> IO.inspect()
# output: [%{group_id: 2, id: 2, name: "Susilo"}]
{{< / highlight >}}

**`map(enumerable, fun)`**

Fungsi `map` berguna untuk menerapakan argumen `fun` ke semua elemen pada argumen `enumerable`. Berbeda dengan fungsi `filter` yang tidak merubah tipe data, fungsi `map` akan merubah tipe data tetapi tidak merubah jumlah elemennya.

{{< highlight elixir >}}
num = [1, 2, 3]
Enum.map(num, fn x -> x * 2 end) |> IO.inspect()
# output: [2, 4, 6]
{{< / highlight >}}

Contoh untuk data list map, dapat dilihat bahwa bentuk data berubah dari map menjadi tuple.

{{< highlight elixir >}}
user = [
  %{id: 1, group_id: 1, name: "Agus"},
  %{id: 2, group_id: 2, name: "Susilo"}
]
Enum.map(user, fn x -> {x.id, x.name}) |> IO.inspect()
# output: [{1, "Agus"}, {2, "Susilo"}]
{{< / highlight >}}

**`reduce(enumerable, acc, fun)`**

Fungsi `reduce` akan merubah jumlah elemen pada hasil dan merubah tipe datanya, namun tidak selalu fungsi harus merubah 
tipe data.

{{< highlight elixir >}}
user = [
  %{id: 1, group_id: 1, name: "Agus"},
  %{id: 2, group_id: 2, name: "Susilo"}
]
Enum.reduce(user, [], fn item, acc ->
  if item.id == 1 do
    acc
  else
    acc ++ [{item.id, item.name}]
  end
end) |> IO.inspect()
# output: [{2, "Susilo"}]
{{< / highlight >}}

Pada contoh kode di atas fungsi `reduce` merubah tipe data dari `map` menjadi `tuple` dan merubah jumlah data dari 2 menjadi cuma 1 data saja.

Setelah kita tahu perbedaan dan kapan penggunaan fungsi-fungsi di atas, semoga kode kita dapat lebih berkualitas dan benar.

Sekian, semoga bermanfaat.

