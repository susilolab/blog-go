---
categories: php
date: "2019-08-28T19:44:42Z"
title: Memanggil action controller tanpa request ke http di Yii2
---

Pernahkan kamu suatu kali ingin menjalankan aksi pada controller tanpa request ke http dengan browser atau curl.
Hanya sekedar ingin mengurangi waktu request ke server atau kamu tidak ingin membuat kode lagi hanya untuk mengambil data dari hasil proses controller tadi. Nah cara tersebut ternyata mudah kita lakukan di Yii2.
<!--more-->

Misal kita ingin memanggil action **MenuProfile** dicontroller **MenuController**
{{< highlight php >}}
<?php
use app\controllers\v1\MenuController;

class SiteController extends Controller
{
	public function actionIndex()
	{
		$app = new MenuController('menu', Yii::$app, ['id' => 'menu']);
		return $app->runAction('menu-profile');
	}
}
{{< / highlight >}}

Variabel `$app` berisi instance klas **MenuController** dengan argumen sesuai dengan konstruktor klas induk `yii\web\Controller` yaitu `public void __construct ( $id, $module, $config = [] )` penjelasan argumennya sebagai berikut:

| `$id`      | id controller misalnya `menu`                                                                                           |
| `$module`  | modul dari controller dan bisa diisi dengan `Yii::$app->module` atau `Yii::$app`                                        |
| `$config`  | bersifat opsional dan isinya berupa variabel public dari klas yang dipanggil atau klas induk dari klas yang dipanggil.  |

Kemudian fungsi **runAction** memiliki 2 buah parameter yaitu **nama action** (nama action formatnya akan dirubah ke lowercase stripped) dan parameter kedua yaitu argumen dari fungsi action yang dipanggil. misalnya seperti ini:

{{< highlight php >}}
<?php
public function actionMenuProfile($id) 
{
	// kode dari menu-profile	
}
{{< / highlight >}}

Maka parameter yang dapat disertakan adalah `['id' => 1]` contoh `$app->runAction('menu-profile', ['id' => 1])`;

sekian.