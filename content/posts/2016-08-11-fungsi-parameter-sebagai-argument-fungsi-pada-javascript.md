---
categories: javascript
date: "2016-08-11T08:00:00Z"
title: Fungsi parameter sebagai argument fungsi pada javascript
---

Javascript adalah bahasa scripting yang berorientasi object yang berjalan pada sisi client browser dan juga Javascript menerapkan function sebagai sebuah object. Bagaimana javascript menangani parameter yang berupa function?
<!--more-->

Nah kali ini saya ingin sedikit berbagi tentang bagaimana membuat fungsi yang argumentnya berupa fungsi, bingung?, penasaran?. Ada dua cara dalam pemanggilan fungsi dalam javascript, yang pertama menggunakan fungsi `call` dan yang kedua menggunakan fungsi `apply`. Perbedaan dari kedua fungsi tersebut adalah pada parameternya, fungsi `apply` mengharuskan penggunaan array sebagai parameter sedangkan `call` tidak.

Sebelum memanggil fungsi kita perlu memeriksa apakah sebuah variabel itu tipenya berupa fungsi atau tidak, caranya adalah dengan menggunakan fungsi bawaan yaitu `typeof`, fungsi ini akan memeriksa tipe dari sebuah object, apakah berupa string, number, object, function atau tipe yang lain.

Untuk lebih jelasnya bisa dilihat pada contoh kode berikut:

{{< highlight javascript >}}
function greeting(name, filter) {
    if(typeof filter === 'function')
        name = filter.call(this, name);
    console.log(name);
}

// Pemanggilan 1, dengan parameter fungsi
greeting('Agus Susilo', function(name) {
    return name.toUpperCase();
});
// output: AGUS SUSILO

// Pemanggilan 2, tanpa parameter fungsi
greeting('Agus Susilo');// output: Agus Susilo
{{< / highlight >}}

Pada contoh di atas saya membuat fungsi sederhana yang akan mencetak nama dengan dua buah parameter, parameter yang pertama adalah name bertype string dan parameter kedua berupa fungsi atau diharapkan fungsi namun jika parameter kedua bukan fungsipun fungsi akan tetap mencetak name.

Parameter kedua ini diperiksa terlebih dahulu dengan `typeof`, jika berupa fungsi maka fungsi yang dilewatkan pada parameter kedua akan dipanggil dengan method call, parameter this pada call adalah si pemanggil fungsi.

Nah mudah bukan, semoga bermanfaat.