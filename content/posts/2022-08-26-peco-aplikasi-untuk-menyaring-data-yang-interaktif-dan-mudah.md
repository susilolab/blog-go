---
categories: linux
date: "2022-08-26T05:00:00Z"
title: Peco aplikasi untuk menyaring data yang interaktif dan mudah
tags:
- linux
- cli
---

![peco tool](/img/peco-tool.gif)

Jika kamu adalah seorang devops/programmer yang suka dengan cli maka tool ini akan sangat cocok sebagai aplikasi harian 
yang akan memudahkanmu dalam menyaring data dari proses lain ataupun dari file log dan sejenisnya. Biasanya saya akan menggunakan 
perintah grep untuk menyaring data dari proses `ps ax` saat mencari sebuah proses. Nah dengan aplikasi `peco` bagaimana?.
<!--more-->

Dengan menggunakan `peco` hal-hal seperti menyaring data dari perintah `ls`, `ps ax`, `cat /var/log/nginx/error.log | grep 404` akan menjadi sangat mudah dan interaktif.
Contoh saat kita ingin menyaring data dari proses `ps ax | grep php`.

![peco tool](/img/peco-tool.png)

Bisa dilihat pada gambar di atas saat kita mengetik kata `php` peco langsung menyaring semua baris yang mengandung kata `php`. Masya Allah sangat membantu :)

## Instalasi

Kamu bisa install peco dengan perintah berikut ini sesuai dengan OS atau distribusi linux jika menggunakannya.

macOS

`brew install peco`

Debian/Ubuntu

`sudo apt install peco`

Archlinux

`sudo pacman -S peco`

Sekian semoga bermanfaat.

