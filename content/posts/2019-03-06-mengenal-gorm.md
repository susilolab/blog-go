---
categories: go
date: "2019-03-06T08:00:00Z"
title: Mengenal Gorm
---

Gorm adalah pustaka ORM untuk golang, ORM itu singkatan dari object-relational mapping walaupun sebenarnya golang bukan bahasa OOP seperti pada umumnya. Alasan kenapa saya menggunakan gorm adalah karena kita tidak perlu merubah tipe data dari mysql ke struk, apalagi jika field struk tersebut ada yang null maka kita harus menggunakan tipe data yang nullable.
<!--more-->

Contohnya string nullable, int nullable dll. Akan sangat repot sekali jika kolomnya banyak, oleh karena itu saya memakai gorm untuk memudahkan hal tersebut.

#### Install

{{< highlight bash >}}
$ go get github.com/jinzhu/gorm
{{< / highlight >}}

Untuk contoh saya akan membuat tabel `posts`, silahkan buat tabel posts dengan aplikasi favorit Anda.

{{< highlight sql >}}
create table posts(
  id int(11) not null primary key auto_increment,
  title varchar(100) not null,
  body text,
  published tinyint(1) unsigned default 0
);
{{< / highlight >}}

Setelah kita selesai membuat tabel posts, maka kita akan membuat folder gorm. Karena akun github saya adalah susilolab maka saya akan membuat folder di `github.com/susilolab/blog/gorm` dan kita membuat file `main.go`.

#### Koneksi ke database mysql

kita akan mencoba menghubungkan ke database mysql. Kita juga membutuhkan pustaka driver mysql selain gorm untuk menghubungkan ke database. Ok, pertama kita bikin paket main dan import paket `fmt`, `log`, driver `mysql` dan `gorm`.

Dan buat struk `Post`, struk ini untuk mewakili dari table `posts`. Struk pada Go konsepnya mirip klas pada PHP. Berikut potongan kodenya:

{{< highlight go >}}
package main

import (
    "fmt"
    "log"

    _ "github.com/go-sql-driver/mysql"
    "github.com/jinzhu/gorm"
)

type Post struct {
    ID        int
    Title     string
    Body      string
    Published int
}
{{< / highlight >}}

#### Koneksi database

Kemudian pada fungsi main kita buat koneksi ke database dengan memanggil fungsi `gorm.Open`, fungsi `Open` akan mengembalikan struk `gorm.DB` dan `err`. Jika err tidak sama dengan nil munculkan pesan error dan exit, cukup tuliskan `log.Fatalln`. `Defer.Close()` setelah error handling berguna untuk menutup koneksi mysql.

{{< highlight go >}}
func main() {
    conn := "root:password@tcp(127.0.0.1:3306)/blog?charset=utf8&parseTime=True"
    db, err := gorm.Open("mysql", conn)
    if err != nil {
        log.Fatalln(err)
    }
    defer db.Close()
}
{{< / highlight >}}

#### Query Data

Kemudian kita bisa memulai query data, kita akan mencoba mengambil semua data yang ada pada tabel `posts`, perlu di ingat bahwa gorm akan menggangap nama tabel semua dalam bentuk jamak kecuali kita mengaktifkan mode singular atau menyebutkan nama tabel dengan fungsi Table. Misalnya struk `User` maka nama tabelnya akan dianggap `users`, struk `UserGroup` namanya tabelnya akan dianggap `user_groups` dll. Pertama kita bikin slice `Post` dengan nama `posts` untuk menampung datanya.

{{< highlight go >}}
posts := make([]Post, 0)
if db.Find(&posts).RecordNotFound() {
    log.Fatalln("record tidak ada")
}

for _, val := range posts {
    fmt.Println(val.Title)
}
{{< / highlight >}}

#### Mendapat data pertama dan terakhir

Untuk mendapatkan record pertama kita bisa menggunakan fungsi First dan untuk data terakhir menggunakan fungsi Last

{{< highlight go >}}
var post Post
db.First(&post)
db.First(&post, 10)

var lastPost Post
db.Last(&lastPost)
{{< / highlight >}}

#### Insert data ke tabel

Untuk insert data ke tabel ada 2 cara, pertama menggunakan fungsi `Create` dan yang kedua dengan fungsi `NewRecord`. Fungsi `Create` akan mengembalikan struk `gorm.DB`, untuk melihat errornya bisa melalui property `Error`. Sedangkan `NewRecord` akan mengembalikan nilai true jika sukses dan false jika gagal.

Pertama buat struk Post dan isi field-fieldnya.

{{< highlight go >}}
Post := Post{ID: 2, Title: "belajar gorm", Body: "awesome", Published: 1}
if err := db.Create().Error; err == nil {
    fmt.Println("data sukses disimpan.");
}
{{< / highlight >}}

#### Update

Update dapat dilakukan dengan memanggil fungsi `Save`, setelah kita mencari recordnya terlebih dahulu.

{{< highlight go >}}
post := Post{}
if db.First(&post, 1).RecordNotFound() {
  log.Fatalln("Record not found.")
}

post.Title = "Belajar ORM dengan Gorm"
db.Save(&post)
{{< / highlight >}}

Sekian dari saya dan semoga bermanfaat. Untuk dokumentasi lengkapnya dapat Anda baca disini [gorm.io](https://gorm.io).
