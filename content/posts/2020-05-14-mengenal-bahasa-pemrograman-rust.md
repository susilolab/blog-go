---
categories: rust
date: "2020-05-14T08:00:00Z"
title: Mengenal Bahasa Pemrograman Rust
---

Rust adalah bahasa pemrograman multi-paradigma yang fokus pada performa dan keamanan terutama `safe concurrency`, 
Rust secara syntax mirip C++ tetapi menyediakan kemanan memory tanpa menggunakan Garbage Collector pada pengelolaan memorinya. (wikipedia)
<!--more-->

Menurut website resmi rust-lang.org yang lama, rust adalah
> Rust is a systems programming language that runs blazingly fast, prevents segfaults, and guarantees thread safety. 

itu artinya Rust berjalan sangat cepat, mencegah segfaults dan menjamin keamanan thread tanpa garbage collector.

dengan beberapa fitur seperti
* zero-cost abstractions  
* move semantics  
* guaranteed memory safety  
* threads without data races  
* trait-based generics  
* pattern matching  
* type inference  
* minimal runtime  
* efficient C bindings  

### Menginstall Rust

Salah satu yang membuat saya tertarik termasuk pada instalasinya yang sangat mudah dan rust juga menyediakan [`Rust Playground`](https://play.rust-lang.org/) sebagai tempat mencoba kode rust tanpa harus menginstal dikomputermu.

#### Rustup: Installer rust dan alat pengelola versi

Cara install paling adalah mudah adalah menggunakan rustup yang merupakan installer resmi dari rust-lang. Installer ini mendukung 3 sistem operasi seperti linux, mac dan windows. Jika kamu menggunakan sistem operasi berbasis unix, cukup copas kode berikut ke terminal Anda

{{< highlight bash >}}
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
{{< / highlight >}}

Jika kamu menggunakan windows 64bit download dan jalankan file berikut [rustup-init.exe](https://win.rustup.rs/x86_64)

Jika kamu menggunakan windows 32bit download dan jalankan file berikut [rustup-init.exe](https://win.rustup.rs/i686)

Setelah instalasi selesai kamu bisa memeriksa di terminal untuk memastikan apakah Rust sudah terinstall atau belum dengan mengetik perintah berikut ini, jika outputnya seperti pada gambar berikut ini maka instalasi Rust sukses.
![rustup-show](/img/20200406-rustup-show.png)

#### Hello World

Untuk pengenalan dasar rust kita akan membuat aplikasi sederhana yang hanya akan mencetak tulisan `Hello World` pada konsole.

Buat file dengan `hello.rs` kemudian ketikan kode seperti berikut ini.

{{< highlight rust >}}
fn main() {
    println!("Hello World");
}
{{< / highlight >}}

Kemudian kompile dengan mengetikan perintah berikut ini pada terminal.

{{< highlight bash >}}
$ rustc hello.rs
{{< / highlight >}}

Lalu jalankan dengan perintah

{{< highlight bash >}}
$ ./hello
$ Hello World
{{< / highlight >}}

#### Cargo

Untuk aplikasi sederhana penggunaan file tunggal tidak ada masalah namun untuk aplikasi besar disarankan untuk menggunakan aplikasi bernama `cargo`. `cargo` adalah alat untuk membangun aplikasi dan pengelola paket bawaan dari Rust. Dengan adanya cargo membuat aplikasi kecil ataupun besar menjadi mudah.

Nah untuk pengenalan kita akan membuat aplikasi sederhana dengan menggunakan cargo dan aplikasi ini hanya akan mencetak `Hello, world!`.

Ok berikut ini perintah dari cargo yang akan kita gunakan
* `new` untuk membuat project baru
* `run` untuk menjalankan project

Buat project dengan nama `hello_rust` dengan mengetik perintah berikut ini ke terminal
{{< highlight bash >}}
$ cargo new hello_rust
{{< / highlight >}}

Maka cargo akan mengenerate-kan project seperti ini
{{< highlight bash >}}
hello_rust
|-- Cargo.toml
|-- src
   |-- main.rs
{{< / highlight >}}

`Cargo.toml` adalah file manifest dari Rust, dimana menyimpan metadata project kita dan dependecy-nya.

`main.rs` adalah file dimana aplikasi kita akan ditulis.

Kita langsung bisa menjalankan aplikasi dengan mengetik
{{< highlight bash >}}
$ cargo run
   Compiling hello_rust v0.1.0 (/tmp/hello_rust)
    Finished dev [unoptimized + debuginfo] target(s) in 0.63s
     Running `target/debug/hello_rust`
Hello, world!
{{< / highlight >}}

Cukup sekian dari saya tentang pengenalan bahasa pemrograman Rust, semoga bermanfaat.

Jika kamu berkenan membaca artikel dengan bahasa inggris saya sarankan langsung ke [website resmi rust](https://www.rust-lang.org/learn) untuk mempelajari lebih lanjut tentang rust


Wassalam.
