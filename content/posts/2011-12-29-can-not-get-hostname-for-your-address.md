---
categories: database
date: "2011-12-29T08:00:00Z"
title: Can't get hostname for your address
---

Malam-malam pas lagi butuh akses MySQL, e malah error `Cannot get hostname for your address`. Lalu cari-cari di google nemu solusi begini.
<!--more-->

Stop MySQL server. Terus edit file `my.cnf` atau `my.ini`. Kemudian tambahkan baris `skip-name-resolve` pada section `[mysqld]`. Jalankan MySQL server kembali. nah mysql Anda sudah bisa diakses lagi :).