---
categories: linux
date: "2022-09-05T05:00:00Z"
title: Highlight neovim tidak jalan di tmux
---

![vim+tmux](/img/vim-tmux-02.png)

Jika highlight line pada vim/neovim kamu tidak jalan pada saat menggunakan tmux kamu bisa edit confignya seperti ini:
<!--more-->

Komen config neovim jika di tempat saya berlokasi di `~/.config/nvim/init.vim` pada keyword `set termguicolors` dengan menggunakan `"`

{{< highlight vim >}}
" set termguicolors
{{< / highlight >}}

Kemudian tutup dan buka kembali terminal kamu dan cobalah untuk menjalankan tmux lalu vim maka highlightnya akan berjalan kembali

![vim+tmux](/img/vim-tmux-01.png)

