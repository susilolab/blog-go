---
categories: elixir
date: "2019-09-03T15:00:00Z"
title: Run command di Elixir
---

Cara menjalankan command cli di elixir bisa langsung dicoba di`iex` atau buat file elixir script(`.exs`)
<!--more-->

{{< highlight elixir >}}
iex(1)> System.cmd("ls", ["/home/agus"])
{"Desktop\nDocuments\nDownloads\nMusic\nPictures\nPublic\nTemplates\nVideos\n",
 0}
{{< / highlight >}}

Perintah `cmd` ini akan mengembalikan koleksi `tuple hasil` dan `kode exit`. Jika command yang dijalankan tidak ada didalam var **PATH** maka elixir akan menampilkan error seperti ini:

{{< highlight elixir >}}
** (ErlangError) Erlang error: :enoent
    (elixir) lib/system.ex:791: System.cmd("ls1", ["/home/agus"], [])
{{< / highlight >}}

Untuk mengolah hasil dari `cmd` bisa menggunakan patter matching elixir, kemudian split file menggunakan fungsi `String.split`

{{< highlight elixir >}}
iex(1)> {res, exit_code} = System.cmd("ls", ["/home/agus"])
iex(2)> res |> String.split
["Desktop", "Documents", "Downloads", "Music", "Pictures", "Public",
 "Templates", "Videos"]
{{< / highlight >}}

Nah setelah displit maka hasilnya sudah berupa `list` dan dapat diproses oleh fungsi lain dengan mudah.

sekian.
