---
categories: 
- php
- Programming
date: "2023-08-20T05:00:00Z"
title: Fix style kodingmu di PHP dengan php cs fixer
---

Pusing mikir style koding yang berantakan?. ndak usah khawatir, fokus saja pada logik kodemu dan serahkan stylenya pada php coding style fixer.

<!--more-->

Komen config neovim jika di tempat saya berlokasi di `~/.config/nvim/init.vim` pada keyword `set termguicolors` dengan menggunakan `"`

{{< highlight vim >}}
" set termguicolors
{{< / highlight >}}

Kemudian tutup dan buka kembali terminal kamu dan cobalah untuk menjalankan tmux lalu vim maka highlightnya akan berjalan kembali

![vim+tmux](/img/vim-tmux-01.png)
