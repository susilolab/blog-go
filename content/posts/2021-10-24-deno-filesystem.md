+++
categories = 'deno'
date = "2021-10-24T07:00:00Z"
tags = ['deno', 'typescript']
title = 'Deno File System'
+++

Deno adalah secure runtime untuk Javascript dan Typescript menggunakan javascript engine v8 buatan Google dan juga
dibangun dengan bahasa pemrograman Rust. Pembuatnya adalah orang yang sama yang membuat Nodejs. 
Versi stabilnya dirilis pada tanggal 13 Mei 2020.

Pada kesempatan kali sesuai dengan judul artikel yaitu tentang file sistem di Deno, kita akan mengulik sedikit tentang 
file system di Deno seperti membaca dan menulis file, membuat dan menghapus folder, menyalin file/folder dll.
<!--more-->

Sebelum masuk ke bahasan utama alangkah lebih baiknya jika mengenal cara menjalankan deno dulu, jadi untuk menjalankan typescript dengan 
deno gunakan command `deno run <nama-script>`. Mungkin agak sedikit asing dan berbeda dengan `nodejs` yang tidak memerlukan perintah kedua 
yaitu `run`.

Namun bagiku hal itu akan mempermudah dan membedakan dengan command lain mengingat deno menyertakan banyak command dalam satu binary.
Seperti `deno fmt`, `deno doc`, `deno lint`, `deno info` dan lain-lain. Berikut daftar lengkapnya

{{< highlight bash >}}
$ deno run nama-script.ts/nama-script.js
{{< / highlight >}}

Deno juga mengharuskan kita untuk menyebut permisi apa saja pada saat menjalankan script seperti `--allow-read` jika didalam 
script terdapat pembacaan file, `--allow-write` jika terjadi penulisan ke file dll.

| Opsi            | Penjelasan                                                                                            |
| --------------- | ------------------------------------------------------------------------------------------------------|
| -A, --allow-all | Mengizinkan semua permisi. Ini menonaktifkan permisi                                                  |
| --allow-env     | Mengizinkan akses environment                                                                         |
| --allow-hrtime  | Mengizinkan pengukuran waktu resolusi-tinggi                                                          |
| --allow-net     | Mengizinkan akses jaringan                                                                            |
| --allow-plugin  | Mengizinkan plugin. Catat bahwa `--allow-plugin` fitur tidak stabil                                   |
| --allow-read    | Mengizinkan akses pembacaan ke sistem file                                                            |
| --allow-write   | Mengizinkan akses ke penulisan sistem file. kamu juga dapat menyebutkan nama file/folder yang diakses |
| --allow-run     | Mengizinkan menjalankan subprocess                                                                    |

Contoh penggunaan opsi permisi

{{< highlight bash >}}
$ deno run --allow-read read_file.ts
$ deno run --allow-write write_file.ts
{{< / highlight >}}

**Catatan:** Deno memerlukan tambahan opsi pada saat menjalankannya sebagai contoh `--allow-read` untuk mengijinkan 
deno untuk membaca file/ menyalin file/folder.
{:.note-tip}

### Baca File

Untuk membaca file Deno menyediakan fungsi `readFile` pada namespace Deno. Buat file dengan nama `read_file.ts` 
kemudian salin kode berikut ini:

{{< highlight typescript >}}
const decoder = new TextDecoder('utf-8');
const data = await Deno.readFile('copy_file.ts');
console.log(decoder.decode(data));
{{< / highlight >}}

Karena fungsi `Deno.readFile` menghasilkan data array byte maka perlu didecode menggunakan klas `TextDecoder`.

perlu tambahan opsi `--allow-read`
{:.note-tip}

### Menulis File

Untuk menulis file kita dapat menggunakan fungsi `Deno.writeFile` dimana argumen pertama adalah nama file, kedua data yang bertype
Uint8Array sehingga harus diencode menggunakan klas `TextEncoder` dan argumen terakhir adalah opsi lain seperti mode permisi,
append yang berguna untuk menambahkan data jika file sudah ada.

{{< highlight typescript >}}
const encoder = new TextEncoder();
const data = encoder.encode('Hello world!');
await Deno.writeFile('/tmp/hello.txt', data);
await Deno.writeFile('/tmp/hello.txt', data, { mode: 0o777 });
await Deno.writeFile('/tmp/hello.txt', data, { append: true });
{{< / highlight >}}

memerlukan ijin `--allow-write`, dan `--allow-read` jika opsi `create` bernilai false
{:.note-tip}

### Membuat Folder

Untuk membuat folder kita bisa menggunakan fungsi `Deno.mkdir`, argumen yang wajib disediakan adalah nama folder sedangkan
opsi yang bisa ditambahkan adalah mode permisi dan recursive yang bernilai boolean.
{{< highlight typescript >}}
await Deno.mkdir('/tmp/hello');
await Deno.mkdir('/tmp/hello', { recursive: true });
await Deno.mkdir('/tmp/hello', { mode: 0o777 });
{{< / highlight >}}

memerlukan ijin `--allow-write`
{:.note-tip}

### Menghapus File/Folder

Untuk menghapus file/folder kita bisa menggunakan fungsi `Deno.remove` dimana kita bisa menambahkan opsi recursive atau tidak pada
saat menghapus folder.
{{< highlight typescript >}}
await Deno.remove('/tmp/hello.txt');
await Deno.remove('/tmp/nama_folder');
await Deno.remove('/tmp/nama_folder', { recursive: true });
{{< / highlight >}} 

memerlukan ijin `--allow-write`
{:.note-tip}

### Menyalin File

Untuk menyalin file kita bisa menggunakan fungsi `Deno.copyFile` dengan argumen pertama berupa sumber file dan argumen kedua berupa
tujuan file.
{{< highlight typescript >}}
await Deno.copyFile('/tmp/hello.txt', '/tmp/world.txt');
{{< / highlight >}}

memerlukan ijin `--allow-write` dan `--allow-read`
{:.note-tip}

### Menyalin Folder

Untuk menyalin folder namespace Deno tidak menyediakannya untuk itu kita menggunakan fungsi dari standard
library yaitu `copy` dan menambahkan opsi `--unstable` pada saat menjalankan karena memang library belum stabil.
{{< highlight typescript >}}
import { copy } from "https://deno.land/std@0.54.0/fs/mod.ts";

copy("/home/user/big-project", "/tmp/big-project");
{{< / highlight >}}

memerlukan ijin `--allow-read` dan `--allow-write` dan juga opsi `--unstable`
{:.note-tip}

sekian.
