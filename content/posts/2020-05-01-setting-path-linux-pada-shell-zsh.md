---
categories: linux
date: "2020-05-01T08:00:00Z"
title: Setting PATH linux pada shell zsh
---

`PATH` merupakan variabel shell yang akan menjadi acuan saat kita menjalankan perintah pada terminal dilinux. Shell akan mencari file binary dengan melihat variabel PATH. Misal kita ingin membuat custom command dengan program bash sederhana yang akan menjalankan aplikasi composer tanpa harus harus menyebut command php. contoh:
<!--more-->

simpan dengan nama `composer` dan letakkan di $HOME/bin
{{< highlight bash >}}
#!/bin/bash

php $HOME/.local/bin/composer.phar $*
{{< / highlight >}}

Agar command `composer` yang kita buat di atas dapat dijalankan difolder mana saja maka perlu kita tambahkan pada variabel `PATH`. untuk shell bash bisa kita tambahkan pada file `$HOME/.bashrc` atau `$HOME/.bash_profile` dan untuk shell zsh semua setingan ada pada lokasi `$HOME/.zshrc` atau `$HOME/.zprofile`, Kita tinggal menambahkan dengan syntax seperti berikut ini

{{< highlight bash >}}
export PATH=$HOME/bin:$PATH
{{< / highlight >}}

Kemudian agar zsh/ bash mereload file konfigurasi baru gunakan perintah `source ~/.zshrc`.

sekian