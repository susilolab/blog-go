---
categories: linux
date: "2020-11-12T06:00:00Z"
title: Menutup branch pada Mercurial
---

Pada mercurial jika kita membuat cabang kemudian mem-merge cabang baru dengan cabang default, kita akan tetap mendapat cabang dari revisi yang di push oleh rekan satu tim 
jika kita tidak menutup cabang tersebut. Untuk menutup cabang kita harus menyertakan opsi `--close-branch` pada saat commit setelah merge cabang. Setelah kita tutup, cabang 
tersebut akan hilang dari repo master sesudah kita push
<!--more-->

{{< highlight bash >}}
$ hg commit -m "Tutup cabang" --close-branch -u agus
{{< / highlight >}}

Sekian, semoga bermanfaat

