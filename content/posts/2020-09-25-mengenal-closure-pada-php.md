---
categories: php
date: "2020-09-25T06:00:00Z"
title: Mengenal Closure pada PHP
tags:
- php
---

Assalamu'alaikum, semoga hari Anda menyenangkan karena saya akan mengenalkan apa itu closure, terutama closure
pada PHP. Closure atau bisa disebut juga anonymous function adalah fungsi yang tidak memiliki nama.
closures sangat berguna sebagai nilai dari parameter callback, tetapi juga berguna pada penggunaan lain.
untuk lebih jelasnya kita lihat contoh dibawah
<!--more-->

{{< highlight php >}}
$hello = function() {
    echo 'world';
};

$hello();
{{< / highlight >}}

Untuk memanggil closure gunakan tanda kurung tutup setelah nama variabel. karena closure sebenarnya sama seperti
fungsi pada umumnya, maka kita juga dapat memberikan parameter pada closure.

{{< highlight php >}}
$hello = function($msg) {
    echo $msg . ' world';
};

$hello('hello');
{{< / highlight >}}

Contoh lain penggunaan closures, `array_walk` berguna untuk menerapkan fungsi ke setiap elemen pada array.
jika parameter `$userdata` diisi maka data akan dilewatkan ke fungsi pada parameter ketiga.

{{< highlight php >}}
$fruits = ['a' => 'Apel', 'b' => 'Belimbing', 'c' => 'Cerry'];
array_walk($fruits, function($item, $key) {
    echo $key . '. ' . $item . "\n";
});
{{< / highlight >}}

Nama parameter tidak harus sama dengan contoh diatas tetapi urutan data tetap sama yaitu nilai array,
kunci dan userdata jika ada.

{{< highlight php >}}
array_walk($fruits, function(&$item, $key, $prefix) {
    echo "$key. $prefix $item\n";
}, 'buah');
{{< / highlight >}}

Jika parameter userdata disertakan maka parameter pertama perlu dilewatkan sebagai referensi bukan sebagai nilai.
agar nilainya dapat dirubah oleh userdata.

lihatlah perbedaannya jika tidak menggunakan closure

{{< highlight php >}}
function test_print(&$item, $key, $prefix) {
    echo "$key. $prefix $item\n";
}
array_walk($fruits, 'test_print', 'buah');
{{< / highlight >}}

sama kan?

Perlu diingat bahwa variabel diluar closure tidak dapat diakses, untuk dapat mengakses variabel diluar closure gunakan keyword `use` dan agar variabel diluar closures dapat dirubah didalam closures lewatkan parameter sebagai referensi.
gunakan tanda `&` sebelum parameter use. lihat contoh dibawah ini

{{< highlight php >}}
$products = [ [2, 1000], [3, 2000], [2, 3000]];
$tax = 0.05;

$total = 0.00;
array_walk($products, function($product, $key) use($tax, &$total) {
    $qty   = $product[0];
    $price = $product[1];

    $total += ($qty * $price) * ($tax+1.0);
});

echo round($total, 2);
{{< / highlight >}}

Terlihat bahwa variabel `$tax` dapat dibaca didalam closures, dan juga variabel `$total` dapat berubah nilainya.

sekian dari saya, semoga bermanfaat. jika ada punya pemikiran lain silahkan merespon :)
