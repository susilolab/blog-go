---
categories: php
date: "2022-08-25T06:00:00Z"
title: Menggunakan db selain default pada Yii2 migration
tags:
- php
- yii
---

![yii framework](/img/yii-logo.svg)

Secara default Yii 2 migration menggunakan database dengan nama `db` hal ini dapat dicek pada config database dibawah subkey `components` 
namun jika kita memiliki banyak koneksi maka kita harus menyebutkan database mana yang akan dipakai pada fungsi `init` pada tiap file migrate.
<!--more-->

Untuk merubah koneksi database ke selain default maka kita perlu mensetnya pada fungsi `init` pada tiap file migrate. Misalnya kita ingin merubah 
koneksi dari default ke `db1` maka kita bisa set ke property `db`. berikut kodenya:

{{< highlight php >}}
<?php

use yii\db\Migration;

class m161220_091743_create_user_table extends Migration
{
    public function init()
    {
        $this->db = 'db1';
        parent::init();
    }

    // public function safeUp() {}
    // public function safeDown() {}
}
{{< / highlight >}}

Dengan begitu maka pada saat migrate dijalankan operasi akan dilakukan ke nama database yang sesuai dengan informasi koneksi yaitu `db1`. sekian semoga 
bermanfaat.

