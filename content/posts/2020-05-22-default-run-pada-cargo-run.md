---
categories: rust
date: "2020-05-22T08:00:00Z"
title: Default run pada cargo run
---

Sebelumnya pada postingan tentang [Cargo run multi binary]({% post_url 2020-04-18-cargo-run-multi-binary %}) saya mencontohkan bagaimana menjalankan binary khusus menggunakan `cargo run`, sehingga kita bisa bikin aplikasi secara terpisah berdasarkan nama file.

Kenapa menggunakan multi binary pada saat menjalankan `cargo run` ?. Karena code `fn main()` dapat digunakan pada setiap file.
<!--more-->

Nah pada artikel sebelumnya saya tidak menjelaskan bagaimana kalau kita menjalankan `cargo run` tanpa parameter `--bin nama`?, ternyata cargo akan menampilkan pesan error bahwa tidak ada default run binary yang harus dijalankan ketika parameter `--bin` tidak disertakan.

{{< highlight bash >}}
$ cargo run
error: `cargo run` could not determine which binary to run. 
Use the `--bin` option to specify a binary, or the `default-run` manifest key.
available binaries: main, basic, simple_server
{{< / highlight >}}

Untuk itu kita perlu mendefinisikan juga untuk file `main.rs` sebagai default jika kita menjalankan `cargo run` tanpa parameter `--bin`.

{{< highlight toml >}}
[package]
name = "rabbit"
version = "0.1.0"
authors = ["Name <name@example.com>"]
edition = "2018"
default-run = "main"

[[bin]]
name = "main"
path = "src/main.rs"

[[bin]]
name = "publisher"
path = "src/publisher.rs"

[[bin]]
name = "subscriber"
path = "src/subscriber.rs"
{{< / highlight >}}

Dapat dilihat bahwa kita menambahkan keyword `default-run` pada section `package` untuk memberitahu cargo file mana yang akan dijadikan bootstrap pada saat perintah `cargo run` dijalankan tanpa parameter `--bin`. Dan juga membuat definisi khusus untuk file `main.rs` dengan nama `main`.

{{< highlight toml >}}
[package]
default-run = "main"

[[bin]]
name = "main"
path = "src/main.rs"
{{< / highlight >}}

Setelah kita menambahkan default-run maka cargo tidak akan komplain pada saat menjalankan `cargo run`.

{{< highlight bash >}}
$ cargo run     
Finished dev [unoptimized + debuginfo] target(s) in 0.06s
Running `target/debug/main`
Hello, world!
{{< / highlight >}}

Sekian.