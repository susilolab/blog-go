---
categories: linux
date: "2020-09-24T07:00:00Z"
title: Status service systemd Elixir activating terus
---

![elixir-systemd](/img/20200806-elixir-systemd.png)

Jadi ini ceritanya saya ada pekerjaan untuk merubah salah satu fungsi pada aplikasi di kantor ke bahasa Elixir,
pada saat aplikasi sudah siap dideploy ke server production status servisnya tidak normal, selalu `activating` terus.
Sebelumnya saya mencoba mengirim data ke servis untuk memastikan apakah aplikasinya jalan apa tidak. Ternyata aplikasinya jalan,
itu artinya tidak ada masalah diaplikasi.
<!--more-->

Kemudian saya coba otak-atik service di systemdnya, setelah beberapa lama baru ketahuan setelah tak coba didatacenter.
Penyebabnya ada pada `type` service pada file konfigurasinya. Awalnya saya pakai type `forking` dan setelah ganti ke type `simple`,
Alhamdulillah normal.

{{< highlight conf >}}
[Unit]
...

[Service]
Type=simple
...

[Install]
WantedBy = multi-user.target
{{< / highlight >}}

![elixir-systemd](/img/20200924-elixir-systemd.png)

Alasan kenapa setelah saya ganti typenya ke `simple` menjadi normal karena type simple cocok jika aplikasi yang akan dijadikan
service memiliki karakteristik aplikasinya jalan dan prompt tidak mengembalikan apa-apa sampai kita tekan `Ctrl+C` atau menghentikan servisnya.

Type `forking` cocok jika karakteristik aplikasi pada saat jalan langsung mengembalikan sesuatu dan servis tetep jalan.

Sekian, semoga bermanfaat.
