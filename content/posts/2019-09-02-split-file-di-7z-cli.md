---
categories: linux
date: "2019-09-02T08:00:00Z"
title: Memecah file dengan 7z cli dilinux
---

Kadang kita butuh memecah file menjadi beberapa bagian agar dapat disalin ke flash disk terutama flash disk yang formatnya fat32. Fat32 hanya mampu menyalin file maksimal file berukuran 4GB. Berikut ini cara memecah file menggunakan 7z cli di linux: 
<!--more-->

{{< highlight bash >}}
$ 7z a -y -mx=0 -v2500m namafile.7z folder_berkas/
{{< / highlight >}}

Penjelasan parameternya

| a              | Perintah menambah berkas pada 7z                                      |
| -y             | Kita paksa 7z agar mengiyakan semua konfirmasi                        |
| -mx=0          | Tingkatan kompresi antara 0 s/d 7. 0 brarti tanpa kompresi            |
| -v2500m        | Berapa ukuran tiap file yang dipecah. ukuran bisa dalam b, k, m dan g |
| nama_file.7z   | Nama file yang diinginkan                                             |
| folder_berkas/ | Folder yang akan dipecah                                              | 

Khusus parameter `-v` gunakan ukuran dibawahnya misal kita ingin memecah file menjadi 2G perfile maka gunakan ukuran `-v1900m`

semoga bermanfaat
