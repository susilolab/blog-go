---
categories: javascript
date: "2021-07-16T07:00:00Z"
title: Mengenal async await pada JavaScript
---

Assalamu'alaikum, bagaimana kabar teman-teman semua? semoga selalu dalam keadaan sehat wal'afiat.
Pada kesempatan kali ini saya ingin mengenalkan penggunaan fungsi async dan keyword await di JavaScript. 
Fitur ini ditambahkan pada ECMA2017, kedua fitur tersebut merupakan pemanis syntax dari `promise` yang 
membuat kode asynchronous lebih mudah ditulis dan dibaca. Kedua fungsi tersebut membuat kode terlihat lebih seperti 
kode synchronous(kode lama yang biasa teman-teman tulis).
<!--more-->

# Dasar dari async/await

Pertama tama untuk membuat fungsi menjadi kita harus letakkan keyword `async` didepan definisi fungsi

Coba ketikan baris berikut ke JS console di browser
{{< highlight javascript >}}
function hello() { return 'Hello' };
hello()
{{< / highlight >}}

Fungsi mengembalikan `Hello`, tidak ada yang spesial kan?

Tetapi bagaimana jika ubah ke fungsi async?, coba baris berikut
{{< highlight javascript >}}
async function hello() { return 'Hello' };
hello()
{{< / highlight >}}

Fungsi yang dipanggil mengembalikan `promise`. Ini adalah salah satu dari async function trait, mereka 
mengembalikan nilai yang digaransi akan diubah ke promise.

Kamu juga bisa membuat async function seperti ini
{{< highlight javascript >}}
let hello = async function() { return 'Hello' };
hello()
{{< / highlight >}}

Atau menggunakan arrow function
{{< highlight javascript >}}
let hello = async () => { return 'Hello' };
hello()
{{< / highlight >}}

Semua kode diatas (anonymous dan arrow function) melakukan hal yang sama.

Untuk mengambil nilai dari promise kita bisa menggunakan blok `.then()`
{{< highlight javascript >}}
hello().then(value => console.log(value))
{{< / highlight >}}

Atau pendeknya
{{< highlight javascript >}}
hello().then(console.log)
{{< / highlight >}}

Jadi keyword `async` itu ditambahkan ke fungsi untuk memberi tahu mereka untuk mengembalikan promise 
dari pada nilai

# Keyword await

Keuntungan dari fungsi async hanya menjadi semua tanpa menggabungkannya dengan keyword `await`.
`await` hanya bekerja didalam fungsi async

Contoh
{{< highlight javascript >}}
const hello = async () => {
    return 'Hello'
}
{{< / highlight >}}