---
categories: rust
date: "2020-05-19T03:00:00Z"
title: Passing argument to app when using cargo run
---

Pernahkah membuat aplikasi cli dengan clap kemudian ingin menjalankan dengan `cargo run --help` tetapi output yang keluar malah helpnya dari cargo ?. Ternyata cargo punya syntax agar argument yang kita lewatkan ditujukan ke aplikasi bukan untuk cargo. Cukup tambahkan `--` sebelum argument yang ada pada aplikasi.
<!--more-->

Contoh:
{{< highlight bash >}}
$ cargo run -- --help
$ cargo run -- --config=app.toml -v
{{< / highlight >}}

Maka argument setelah setelah tanda `--` hanya akan diproses oleh aplikasi yang kita kembangkan.

Sekian, semoga bermanfaat.
