---
categories: linux
date: "2020-03-23T08:00:00Z"
title: Menggunakan Gnome Terminal sebagai default shell pada Tortoisehg
---

Tortoisehg merupakan front-end GUI untuk *mercurial* dan disini saya ingin berbagi bagaimana cara menseting `gnome-terminal` sebagai default shell yang sebelumnya menggunakan default `xterm`.
<!--more-->

Caranya cukup mudah, buka Tortoisehg kemudian klik menu `File -> Settings` kemudian klik ListBox `TortoiseHg` lalu pada inputbox berlabel `Shell` ketikan `gnome-terminal %(root)s. %(reponame)s` kemudian klik Ok. tutup tortoisehg Anda dan buka lagi.
![mencoba-setingan-baru](/img/02-20200325-gnome-shell-tortoisehg.png)

Untuk mencobanya klik kanan pada salah satu repo di `Repostory Registry` kemudian klik terminal, jika tidak ada kendala gnome-terminal akan muncul.
![mencoba-setingan-baru](/img/01-20200325-gnome-shell-tortoisehg.png)

Sekian