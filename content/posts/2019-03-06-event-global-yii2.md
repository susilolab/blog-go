---
categories: php
date: "2019-03-06T10:14:42Z"
title: Event Global Yii2
tags:
- php
- Yii
---

Event pada `yii2` telah ditulis ulang dari versi sebelumnya dan disempurnakan sehingga lebih mudah dan dapat digunakan secara global. Nah kali ini saya ingin berbagai sedikit tip bagaimana membuat event secara global di Yii2. Saya asumsikan bahwa kamu sudah memiliki pengetahuan tentang dasar-dasar **PHP7** dan **Yii2**.

Secara sederhana event pada yii2 mirip event pada **jquery** dan cara pemakaiannya pun mirip. Untuk listening pada event tertentu kita gunakan **on** dan untuk mentrigger event menggunakan fungsi **trigger**. Detil fungsi on dan trigger dapat dilihat dibawah ini:
<!--more-->

{{< highlight php >}}
Event::on($class, $name, $handle, $data = null, $append = true);
Event::trigger($class, $name, $event = null);
{{< / highlight >}}

Tiga parameter utama pada fungsi `on` wajib ada, sedangkan selebihnya boleh diabaikan. Karena kita akan fokus pada bagaimana menggunakan event yii2 secara global maka kita hanya akan menggunakan 3 parameter pertama. contoh penggunaan event secara umum seperti berikut:

{{< highlight php >}}
// SiteController.php
...
use yii\base\Event;

...
const EVENT_HELLO = 'hello';

public function init()
{
  parent::init();
  Event::on(self::className(), self::EVENT_HELLO, [self::className(), 'helloLog']);
}

public function actionIndex()
{
  $event = new Event();
  Event::trigger(self::className(), self::EVENT_HELLO, $event);
  return $this->render('index');
}

public static function helloLog()
{
  Yii::error('---Hello world---'); 
}
...
{{< / highlight >}}

Pada contoh di atas kita mengimport klas `Event` kemudian membuat konstanta nama event. Lalu pada fungsi `init` kita mendaftarkan event melalui fungsi `on` dengan nama fungsi yang akan dijalankan setelah event ditrigger yaitu `helloLog`. Selanjutnya pada fungsi `actionIndex` kita mentrigger event `EVENT_HELLO` memakai fungsi trigger.

Perhatikan parameter ketiga, karena fungsi `helloLog` bertipe static maka nama klasnya menggunakan `self::className`. Jika menggunakan fungsi yang bukan static kita bisa menggunakan `$this` seperti ini:


{{< highlight php >}}
Event::on(self::className(), self::EVENT_HELLO, [$this, 'helloLog']);
{{< / highlight >}}

Kemudian jalankan dengan mengakses controller site, jika tidak ada error maka akan ada log error dengan pesan hello world pada file `runtime/logs/app.log` seperti berikut ini:

![event-global](/img/001-2019-03-06-event-globalyii2.png)

Semoga dengan penjelasan dan contoh di atas kita sudah bisa memahami dasar-dasar event pada `yii2`, sekarang kita bikin event listenernya secara global sehingga kita dapat memicu/mentriggernya dari mana saja(model/controller/module). Agar event dapat didaftarkan secara global pada saat aplikasinya jalan maka kita akan menggunakan BootstrapInterface. kita akan bikin klas implementasi dari BootstrapInterface.

Pertama kita bikin klas dengan nama `EventAutoLoader.php` pada folder `nama_app/components`, Klas ini berguna agar aplikasi dapat memuat konfigurasi event-event pada saat proses booting aplikasi. klas ini turunan dari `BootstrapInterface` yang hanya memiliki satu definisi fungsi yang harus di implementasikan pada klas turunan.

{{< highlight php >}}
<?php
namespace app\components;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Event;

class EventAutoLoader implements BootstrapInterface
{
  public function bootstrap($app)
  {
    $eventConfigFile = require(Yii::getAlias('@app') . '/config/event-config.php');
    foreach ($eventConfigFile as $config) {
      Event::on($config['class'], $config['event'], $config['callback']);
    }
  }
}
{{< / highlight >}}

Kemudian buka file `config/web.php` dan tambahkan klas yang baru saja kita buat pada config `bootstrap`.

{{< highlight php >}}
'bootstrap' => [
  ...
  'app\components\EventAutoLoader',
  ...
],
{{< / highlight >}}

Tak lupa buat file dengan nama event-config.php pada folder config untuk menyimpan semua event dan event handler. Dan isinya kurang lebih seperti ini:

{{< highlight php >}}
<?php
use app\controllers\SiteController;

return [
  [
    'class'    => SiteController::className(),
    'event'    => SiteController::EVENT_HELLO,
    'callback' => [SiteController::className(), 'helloLog'],
  ]
];
{{< / highlight >}}

Sampai tahap ini kita sudah mendaftarkan event secara global dan event tersebut dapat kita dari mana saja, karena klas `EventAutoLoader` akan dijalankan pada saat aplikasi booting pertama kali. Untuk memastikan bahwa event dapat dipanggil dari mana saja, kita akan membuat controller baru dengan nama `HelloController` dan mentrigger event yang ada di `SiteController`.

{{< highlight php >}}
<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\base\Event;
use app\controllers\SiteController;

class HelloController extends Controller
{
    public function actionIndex()
    {
      $event = new Event();
      Event::trigger(SiteController::className(), SiteController::EVENT_HELLO, $event);
    }
}
{{< / highlight >}}

Kemudian akses hello controller melalui browser, jika semua lancar maka akan ada pesan Hello world di `app.log`

![event-global](/img/002-2019-03-06-event-globalyii2.png)

Sekian untuk artikel kali semoga bermanfaat. Insya Allah pada artikel berikutnya saya akan membahas sedikit bagaimana melewatkan data pada event yang telah kita buat. Apabila kamu mempunyai saran dan kritik untuk artikel jangan sungkan2 untuk berkomentar dibawah.
