---
categories: rust
date: "2023-01-08T05:00:00Z"
draft: true
title: Contoh Closure sederhana pada Rust
---

Rust memiliki function anonymous yang mirip dengan Ruby yang bisa kita lihat penggunaan 
tanda `|` walaupun tidak 100% sama. berikut ini contoh penggunaan closure/anonymous function 
pada rust.

Closure pada Rust bisa kita definisikan menggunakan format `||` dan `|| {}` jika statementnya lebih dari satu baris atau 
closure memiliki tipe kembalian.
<!--more-->

Contoh closure paling sederhana tanpa adanya argument dan tipe kembalian.

{{< highlight rust >}}
let demo = || println!("Hello closure.");
demo();
{{< / highlight >}}

Contoh closure dengan argument.

{{< highlight rust >}}
let greeting = |name: &str| println!("Hello {name}.");
greeting();
{{< / highlight >}}

Contoh closure dengan argument dan tipe kembalian.

{{< highlight rust >}}
let add = |x: i32, y: i32| -> i32 { x + y };
let res = add(1, 2);
println!("Hasil 1 + 2 = {res}");
{{< / highlight >}}
