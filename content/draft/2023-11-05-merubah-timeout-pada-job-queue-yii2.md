---
categories: php
date: "2023-11-05T05:00:00Z"
draft: true
title: Merubah Timeout pada job queue Yii2
---

Default timeout pada job queue yii2 adalah 60 detik, terkadang proses yang berjalan lebih dari 60 detik. Supaya job queue dapat berjalan lebih dari 60 detik 
maka klas job perlu implements dari interface `yii\queue\RetryableJobInterface`.

<!--more-->

contoh:

{{< highlight php >}}
use yii\base\BaseObject;

class DemoJob extends BaseObject implements \yii\queue\RetryableJobInterface
{
    public function execute($queue)
    {
        echo 'hello world';
    }

    public function getTtr()
    {
        return 120;
    }

    public function canRetry($attempt, $error)
    {
        return false;
    }
}
{{< / highlight >}}
