---
categories: php
date: "2022-05-31T06:00:00Z"
draft: true
title: Mengenal pustaka async Amphp
---

![amphp](/img/20220531-amp-logo.png)

PHP merupakan bahasa pemrograman scripting yang sangat popular untuk pengembangan web, 80% dari webserver jalan dengan bahasa PHP.
Alasan memilih PHP untuk pengembangan web karena salah satunya harga hosting yang relatif terjangkau dan juga deployment yang mudah.
Namun seiring berkembangnya zaman dengan munculnya Nodejs yaitu javascript runtime server maka para developer banyak yang beralih ke Nodejs.
Apa kelebihan Nodejs dibanding dengan PHP?
<!--more-->

Salah salah satu fitur utama yang menjadi keunggulan Nodejs adalah karena Nodejs berjalan secara asynchronous sehingga membuat eksekusi script menjadi 
lebih cepat bahkan sangat cepat walaupun Nodejs cuma single thread. Apakah PHP juga bisa berjalan secara asynchronous?. Jawaban sederhananya bisa, 
namun karena desain awal PHP tidak diciptakan untuk jalan secara asynchronous maka hal ini butuh usaha yang tidak mudah. Malah aplikasi kita akan 
menjadi sangat komplek.

Namun jangan berkecil hati karena para komunitas developer PHP sudah membuatkan pustaka yang akan membuat kita sedikit lebih mudah dalam mengembangkan aplikasi 
yang berjalan secara asynchronous penuh. Salah satu pustaka yang akan kita bahas kali ini adalah `amphp` sebuah pustaka untuk membangun aplikasi asynchronous.

`Amphp` menyediakan pondasi pustaka untuk membuat aplikasi async, mulai dari `Event Loop`, `Promises`, `Coroutines`, `Iterators` dan `Streams`. `Event Loop` bertugas 
untuk mengatur pembagian tugas dari tiap operasi async. `Promises` sebagai tempat menampung hasil dari operasi async dimana jika pada 
synchronous program memblok sampai hasilnya tersedia, program asynchronous mengembalikan tempat penampung yang mana akan diisi dengan hasil 
operasi async kemudian jika datanya sudah siap.

`Coroutines` adalah fungsi yang dapat diinterupsi yang mana dapat dijeda dan dilanjutkan. Amp menggunakan `Generator` untuk mengkonsumsi promise tanpa callbacks.
`Iterators` berguna untuk membantu memproses data berupa koleksi, jadi data dapat dikonsumsi tanpa harus menunggu semua data siap. `Streams` berguna untuk membuat 
pekerjaan dengan input/output menjadi lebih mudah dalam dunia async seperti baca tulis file.

### Pustaka utama Amp

Berikut ini pustaka utama Amp:

* [amp](https://amphp.org/amp/)
* [byte-stream](https://amphp.org/byte-stream/)
* [cache](https://amphp.org/cache/)
* [serialization](https://amphp.org/serialization/)
* [react-adapter](https://amphp.org/react-adapter/)
* [socket](https://amphp.org/socket/)
* [dns](https://amphp.org/dns/)
* [sync](https://amphp.org/sync/)

Amp juga menyediakan pustakan lainnya yang berhubungan dengan Http server, Http client, Multiprocessing, Persistence dan lain-lain. untuk selengkap bisa dicek dihalaman web [amphp](https://amphp.org)

### Penggunaan

Setelah mengenal teori dari async mari kita lihat bagaimana penggunaan pustaka amphp ini. Karena PHP pada dasarnya merupakan 
sinkron semua dan tidak ada konsep event loop. maka untuk menggunakan pustaka ini harus kita letakkan didalam event loop.
Event loop itu seperti pengatur tugas dari operasi async, jika suatu operasi dijalankan ia tidak menghalangi operasi lain 
untuk berjalan. Jadi kode kita akan berjalan secara concurrent, concurrent bisa kita analogikan seperti seorang chef yang bisa memasak banyak masakan dalam satu waktu tanpa harus menunggu satu masakan selesai dulu baru memasak yang lain.

Kode async harus kita masukan kedalam event loop, untuk menjalankan event loop kita bisa gunakan fungsi `run` pada class `Loop`

{{< highlight php >}}
// hello_async.php
require __DIR__ . '/vendor/autoload.php';

use Amp\Loop;

function tick() {
    echo "tick\n";
}

echo "--sebelum Loop::run()\n";

Loop::run(static function () {
    Loop::repeat(1000, "tick");
    Loop::delay(5000, "Amp\\Loop::stop");
});

echo "--sesudah Loop::run()\n";
{{< / highlight >}}

Jika kita jalankan dengan perintah `php hello_async.php` maka akan tampil dikonsol kamu hasil seperti ini:

```
--sebelum Loop::run()
tick
tick
tick
tick
--sesudah Loop::run()
```

Jika kita perhatikan sekilas tidak ada bedanya dengan kode php yang siknronus. Namun jika kita perhatikan 
dengan lebih seksama maka akan kita temukan perbedaannya yaitu pada fungsi `repeat` yang akan menjadwalkan fungsi `tick` tiap satu detik mencetak 
kata tick yang jika dilakukan pada kode sinkronus tidak bisa karena tidak ada penjadwalan tugas. dan juga pada fungsi `delay` yang menunda eksekusi 
program setelah 5 detik dan kemudian event loop akan berhenti (lihat pada parameter kedua `Amp\Loop::stop`).
