---
categories: rust
date: "2022-03-04T06:00:00Z"
draft: true
title: Konversi tipe di Rust dengan trait From dan Into
---

Datang dari bahasa scripting dan tipe dinamis seperti PHP dan Javascript yang tidak terlalu memperdulikan 
tipe data maka jika kita belajar bahasa seperti Rust akan otomatis peduli dengan tipe data yang kita gunakan 
pada fungsi ataupun struk. Rust sangat ketat dalam tipe data dan banyak hal, sebenarnya Rust punya keyword `as` untuk 
konversi dari tipe data number ke tipe data number lain. Namun untuk tipe data yang komplek kita harus mengimplementasikan 
sendiri dari trait `From` dan `Into`. 
<!--more-->

Contoh penggunaan keyword `as` untuk konversi dari tipe `i32` ke `f64`

{{< highlight rust >}}
fn main() {
    let a = 10;
    let b = 20;
    let c = add(a as f64, b as f64);
    println!("{} + {} = {}", a, b, c);
}

fn add(a: f64, b: f64) -> f64 {
    a + b
}
{{< / highlight >}}

Namun cara di atas tidak disarankan untuk tipe data i64 ke i32 karena akan ada ketidakakuratan disebabkan oleh 
tipe tujuan lebih kecil dari tipe data sumber.
Sebenarnya kita sudah menggunakan trait `From` dan `Into` pada saat menggunakan standard library. 
misalnya pada saat kita ingin membuat variabel `String` dari tipe `str` biasanya menggunakan fungsi `String::from` atau 
`to_string`.

### From

trait From akan mengkonversi tipe data dari sebelah kanan ketipe data sebelah kiri, sebagai contoh misal `String::from("hello")` 
akan mengkonversi tipe `str` (`hello`) ke tipe `String`.

{{< highlight rust >}}
String::from("hello")
^^^^^^        ^^^^^
   |            |
tujuan        sumber
{{< / highlight >}}

Contoh implementasi trait From pada struct

Pertama kita buat struct `Number` yang memiliki field `value` dengan tipe `f64`

{{< highlight rust >}}
#[derive(Debug)]
struct Number {
    value: f64,
}
{{< / highlight >}}

Kemudian kita implementasi trait `From` untuk tipe data `i32`, jadi nantinya struct `Number` dapat menerima 
tipe data `i32`. Kita mengimplementasi trait From ke struct Number dengan keyword `impl` NamaTrait `for` NamaStruct.

{{< highlight rust >}}
impl From<i32> for Number {
    fn from(item: i32) -> Self {
        let value: f64 = item.into();
        Self { value: item }
    }
}
{{< / highlight >}}

Tanda `<` dan `>` merupakan sintak untuk tipe generik karena definisi trait From menggunakan generik tipe `T` 
dan `T` bisa bertipe apa saja.

{{< highlight rust >}}
pub trait From<T>: Sized {
    fn from(_: T) -> Self;
}
{{< / highlight >}}

Lanjut ke detil definisi fungsi from yang menerima satu parameter yang bertipe i32 karena kita akan mengimplementasi 
struk Number dari tipe i32. `Self` itu mewakili nama struk(Number), coba lihat pada `impl ... for Number` karena for nya untuk 
struk Number maka nilai `Self` maka berisi nama struk.

{{< highlight rust >}}
fn from(item: i32) -> Self {
{{< / highlight >}}

Lalu kita konversi nilai dari i32 ke f64 dengan fungsi `into()` karena tipe data f64 mengimplementasi 
trait `From<i32>` maka kita dapat gratis implementasi trait `Into`.

{{< highlight rust >}}
let value: f64 = item.into();
{{< / highlight >}}

Kemudian kita test pada fungsi main dengan membuat variabel num.

{{< highlight rust >}}
fn main() {
    let num = Number::from(10);
    println!("Numberku adalah {:?}", num);
    // Numberku adalah Number { value: 10.0 }

    let int = 5;
    // coba hapus definisi tipe Number
    let num: Number = int.into();
    println!("Numberku adalah {:?}", num);
    // Numberku adalah Number { value: 5.0 }
}
{{< / highlight >}}

### Into

untuk trait `Into` kita tidak disarankan mengimplementasikanya dan lebih disarankan mengimplementasi trait `From`. 
Jika kita sudah mengimplementasi trait `From` maka kita otomatis dapat gratis implementasi trait Into. Contohnya 
dapat dilihat pada kode di atas pada bagian ini.

{{< highlight rust >}}
let num: Number = int.into();
println!("Numberku adalah {:?}", num);
{{< / highlight >}}

