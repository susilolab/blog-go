---
categories: rust
date: "2022-09-18T05:00:00Z"
draft: true
title: Membuat Echo Server TCP dengan Tokio Rust
---

Tokio adalah pustaka untuk membuat aplikasi async di Rust, karena di std lib Rust tidak menyertakan event loop dan lainnya yang berhubungan async await. 
Maka user dapat menggunakan lib dari luar std lib. Pustaka lain yang bisa digunakan adalah `async-std`, namun untuk kali ini kita akan menggunakan tokio 
karena alasan lebih familiar saja.

Kali ini kita akan membuat aplikasi sederhana yaitu TCP Echo Server dan client. Pertama-tama kita buat dulu project baru dengan `cargo`
<!--more-->

Untuk membuat project baru ketikan perintah ini

`cargo new echo_server`

Lalu masuk ke folder `echo_server` dan tambahkan pustaka tokio pada baris setelah `[dependencies]`

```
[dependencies]
tokio = { version = "1", features = ["full"] }
```

Kemudian edit file `src/main.rs`. Pertama kita akan mengimport struk `TcpListener` dan trait `AsyncReadExt` dan `AsyncWriteExt` dari tokio

{{< highlight rust >}}
use tokio::net::TcpListener;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
{{< / highlight >}}

Lalu pada main kita rubah menjadi async agar dapat memproses async function.

{{< highlight rust >}}
use tokio::net::TcpListener;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

#[tokio::main]
async main() -> Result<(), Box<dyn std::error::Error>> {
}
{{< / highlight >}}
