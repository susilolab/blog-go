---
categories: javascript
date: "2021-12-30T06:00:00Z"
draft: true
title: Upload file dengan plain Javascript dan Alpine
---

Kali ini saya ingin berbagi tips bagaimana mengupload file pada saat file input berubah. Disini saya hanya menggunakan pustaka javascript yang bernama 
alpine, tulisan ini saya buat berdasarkan pengalaman setelah membuat form upload pada proyek yang saya buat.
<!--more-->

Pertama-tama buat folder `upload-file` lalu buat file `index.html`. Pertama kita akan membuat form untuk upload file. Ketikan atau copas kode berikut 
di dalam body html.

{{< highlight html >}}
<div x-data="uploadPage">
  <form>
  <input type="file" id="file-name" @change="handleFileUpload($event)" />
  </form>
</div>
{{< / highlight >}}

Disini kita membuat satu buah input dengan tipe `file` dan dengan id `file-name`. Perhatikan atribut `x-data` pada elemen div, `x-data` ini berguna 
untuk menunjukan kepada lib alpine bahwa scope `uploadPage` ada diantara dua buah div. dimana nantinya script akan berinterkasi secara reactive.
Kemudian tambahkan penanganan event pada saat form upload diubah menggunakan syntax `@change`. Syntax ini kependekan dari `x-on:change` dan `$event` akan membawa data ke 
fungsi berupa element input.

Kemudian tambahkan tag script pada baris berikutnya, pada atribut `src` tambahkan alamat lib alpinejs atau nama file dilokal jika lib sudah didownload.
Jangan lupa dengan atribut `defer` atau script tidak akan jalan. 

{{< highlight html >}}
<script type="text/javascript" src="alpine-3.3.4.min.js" defer></script>
{{< / highlight >}}

Kemudian tambah tag script lain untuk logik upload filenya.
Pertama kita akan membuat event listener untuk event `alpine:init`

{{< highlight html >}}
<script type="text/javascript">
'use strict';

document.addEventListener('alpine:init', () => {
})
</script>
{{< / highlight >}}

Didalam alpine:init kita definisikan data scope untuk `uploadPage` dengan syntax `Alpine.data('nama-scope', anom-func)`

{{< highlight javascript >}}
document.addEventListener('alpine:init', () => {
  Alpine.data('uploadPage', () => ({
  }))
})
{{< / highlight >}}

Kemudian kita akan membuat fungsi untuk menghandle upload pada saat file input berubah.

{{< highlight javascript >}}
handleFileUpload(input) {
  const files = input.target.files
  const formData = new FormData()
  formData.append('filename', files[0])

  fetch('index.php', { method: 'POST', body: formData })
  .then(resp => resp.json())
  .catch(err => console.log(err))
}
{{< / highlight >}}

{{< highlight php >}}
<?php
declare(strict_types=1);

$result = [];
foreach ($_FILES as $val) {
    if ($val['error'] != UPLOAD_ERR_OK) {
        continue;
    }
    $tmpName = $val['tmp_name'];
    $filename = basename($val['name']);
    if (move_uploaded_file($tmpName, "public/{$filename}")) {
        $result[] = "public/{$filename}";
    }
}

header('Content-Type: application/json', true, 200);
echo json_encode(['result' => $result]);
{{< / highlight >}}
