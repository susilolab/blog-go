---
categories: rust
date: "2023-11-25T05:00:00Z"
draft: true
title: Membuat gRPC Rust Server dan PHP sebagai Client
---

gRPC adalah framework remote procedure call(rpc) yang dapat berjalan dilingkungan manapun, grpc mendukung banyak bahasa pemrograman. yang membedakan dengan rpc lainnya 
adalah gRPC menggunakan protokol buffer untuk berbagi data antara server dan client yang dikenal sangat cepat dibanding menggunakan json/xml. grpc ini salah satu cara 
yang digunakan developer untuk membuat microservice.

<!--more-->

Didalam gRPC aplikasi client dapat langsung memanggil fungsi di server pada mesin yang berbeda seperti jika fungsi tersebut adalah objek lokal, hal ini membuatmu lebih mudah 
untuk membuat aplikasi dan servis yang terdistribusi.  

Gambaran tentang gRPC bisa dilihat pada diagram dibawah ini.  

![diagram-grpc](/img/grpc-overview.svg)

Dapat dilihat pada gambar bahwa server grpc dibuat dengan bahasa c++ sedangkan 2 client menggunakan Ruby dan Java. Hal ini bisa dilakukan karena masing-masing kode baik 
server maupun client memiliki interface yang sama hasil dari file `.proto`. masing-masing dari server dan client akan digeneratekan kode berdasarkan file `.proto` 
sehingga walaupun hasil generate kodenya berbeda bahasa tetap bisa berkomunikasi.

Dan kali ini saya ingin mengenalkan cara membuat server grpc dengan Rust dan clientnya dengan PHP. Mengapa memilih menggunakan Rust?. Pertama karena saya ingin lebih mendalami Rust, 
yang kedua adalah karena faktor safety (keamanan) bukan kecepatan. Karena di Rust kalau sudah bisa terkompile Insha Allah kode kita aman hehehe.  

Kemudian kenapa untuk client pake PHP karena bahasa ini sudah lama banget saya pake dari saya SMK dan juga tempat saya bekerja masih menggunakan PHP sebagai bahasa utama walaupun 
sekarang sudah campur dengan bahasa lain.  

Jadi saya ingin menguji gRPC ini ditempat saya bekerja agar kode baru pada fitur baru tidak terlalu semrawut.  

Sebelum kita ngoding saya ingin memberikan daftar kebutuhan yang harus diinstall di lokal development baik Rust maupun PHP.

* **protoc** untuk mengenerate stub kode khusus di PHP, karena di Rust tidak perlu protoc ini.
* grpc php plugin 
