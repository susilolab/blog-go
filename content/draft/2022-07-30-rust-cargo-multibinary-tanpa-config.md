---
categories: rust
date: "2022-07-30T05:00:00Z"
draft: true
title: Rust cargo multibinary tanpa config
---

Jika pada artikel saya [sebelumnya]({% post_url 2020-04-18-cargo-run-multi-binary %}) tentang bagaimana cara menjalankan cargo dengan banyak binary 
yaitu proyek cargo dengan file-file yang bisa berjalan sendiri namun kali ini tanpa harus merubah config pada file `Cargo.toml`. Caranya?

<!--more-->

Buat proyek dengan `cargo` seperti biasa misalnya `cargo new --bin hello_world` lalu buat folder `bin` didalam folder `src` seperti ini `hello_world/src/bin`. 
Kemudian buat file rust misalnya `print_number.rs` dan isikan kode sederhana ini

```rust
fn main() {
    println!("angka 1");
}
```

Lalu untuk menjalankannya kita cukup tambahkan parameter pada cargo `--bin nama_file` contoh `cargo run --bin print_number` maka cargo hanya akan 
menjalankan file `print_number.rs` saja. Mudah bukan?

Sekian dan terima kasih.

