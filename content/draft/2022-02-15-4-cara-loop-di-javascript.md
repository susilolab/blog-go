---
categories: javascript
date: "2022-02-15T06:00:00Z"
draft: true
title: 4 Cara For Loop di Javascript
---

Perulangan adalah hal umum dalam sebuah bahasa pemrograman begitu juga dengan javascript. Namun kali ini saya ingin berbagi 
tips tentang 4 perulangan `for` yang berbeda pada Javascript.
<!--more-->

### for

for ini umum digunakan dan developer sangat familiar dari lintas bahasa pemrograman. for ini punya nilai awal, 
kondisi dan ekspresi akhir. Ketiga ekpresi ini opsional bisa diskip semua mulai dari nilai awal, kondisi dan ekpresi akhir.

{{< highlight javascript >}}
for (let i = 0; i < 5; i++) {
  console.log(i)
}

// variasi lain
let i = 0
for (; i < 5; i++) {
  console.log(i)
}

// tanpa ketiga ekpresi
let i = 0
for (;;) {
  if (i > 3) {
    break
  }
  console.log(i)
  i++
}
{{< / highlight >}}

### for in

for in sedikit berbeda dengan for pertama, for in digunakan untuk mengiterasi enumerable. `for...in` tidak disarankan 
untuk array dimana urutan index sangat penting. Untuk penggunaan loop yang lebih baik dengan index angka bisa menggunakan 
`Array.prototype.forEach` atau `for...of`.

{{< highlight javascript >}}
const obj = {a: 1, b: 2, c: 3}
for (const prop in obj) {
  console.log(`obj.${prop} = ${obj[prop]}`)
}
{{< / highlight >}}

### for of

for of membuat iterasi perulangan pada objek yang dapat diiterasi termasuk `String`, `Array`, 
objek seperti array (contoh: `arguments` atau `NodeList`), `TypedArray`, `Map`, `Set` dan iterasi yang dibikin oleh user.

{{< highlight javascript >}}
const array1 = ['a', 'b', 'c']
for (const elm of array1) {
  console.log(elm)
}
{{< / highlight >}}

Iterasi pada String

{{< highlight javascript >}}
const msg = 'foo'
for (const s of msg) {
  console.log(s)
}
// output:
// 'f'
// 'o'
// 'o'
{{< / highlight >}}

Iterasi pada TypedArray

{{< highlight javascript >}}
const iterable = new Uint8Array([0x00, 0xff])
for (const value of iterable) {
  console.log(value)
}
// output:
// 0
// 255
{{< / highlight >}}

### forEach

for ini menempel pada array dan tidak ada break didalam loop kecuali dengan melempar error (throw exception). forEach tidak merubah 
array yang dipanggil.

{{< highlight javascript >}}
const array1 = ['a', 'b', 'c']
array1.forEach(element => console.log(element))
// output:
// 'a'
// 'b'
// 'c'
{{< / highlight >}}

atau dengan index

{{< highlight javascript >}}
const array1 = ['a', 'b', 'c']
array1.forEach((element, index) => {
  console.log(`${index}: ${element}`)
})
// output:
// 0: 'a'
// 1: 'b'
// 2: 'c'
{{< / highlight >}}

Dengan mengetahui variasi for yang berbeda kita dapat menggunakannya sesuai dengan kebutuhan dan dapat memperringkas kode kita.
sekian semoga bermanfaat.

