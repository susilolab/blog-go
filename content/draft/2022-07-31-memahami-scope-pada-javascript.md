---
categories: javascript
date: "2022-07-31T05:00:00Z"
draft: true
title: Memahami scope pada javascript
---

Scope/lingkup pada javascript menjelaskan masa hidup dan visibility suatu variabel. 
Variabel tidak tampak diluar dimana variabel itu dideklarasikan. Javascript mempunyai lingkup module, 
lingkup fungsi, lingkup blok, lingkup lexical dan lingkup global.

<!--more-->

### Lingkup global

Variabel yang dideklarasikan diluar fungsi, blok atau modul mempunyai lingkup global. Variabel 
dilingkup global dapat diakses dari manasaja didalam aplikasi.

Contoh:
`https://medium.com/programming-essentials/an-introduction-to-scope-in-javascript-cbd957022652`
{{< highlight javascript >}}
const POS_END = 1

function hello() {
  console.log(POS_END)
}

class World
{
  run () {
    const scriptPos = getPos()
    if (scriptPos === POS_END) {
      console.log(POS_END)
    }
  }
}
{{< / highlight >}}

### Lingkup module

Variabel yang dideklarasikan diluar fungsi tetapi didalam module tetap disebut variabel global tetapi 
dalam lingkup module dan tersembunyi dari module lain kecuali disebutkan secara eksplisit.

{{< highlight javascript >}}
// user.js
const GROUP_NAME = 'admin'
let name = 'agus'
var users = new User()
{{< / highlight >}}

Dari kode di atas bisa diartikan bahwa const `GROUP_NAME`, variabel `name` dan `users` adalah global terhadap module `user.js` karena dideklarasikan diluar fungsi.
Namun const dan variabel tersebut tidak dapat diakses diluar module `user.js`. Untuk bisa diakses diluar modul kita perlu meng-exportnya.

{{< highlight javascript >}}
// user.js
export const GROUP_NAME = 'admin'
export name = 'agus'
export users = new User()

// home.js
import { GROUP_NAME, name, users } from './user'
{{< / highlight >}}

### Lingkup fungsi

Variabel dan parameter yang dideklarasikan didalam fungsi hanya bisa diakses didalam fungsi, tidak bisa diakses diluar fungsi. Namun terdapat perbedaan pada saat menggunakan `var` dan `let`. 
perhatikan fungsi di bawah yang auto-execute yang biasa disebut IIFE.

{{< highlight javascript >}}
(function () {
  let x = 1 

  const foo = () => {
    console.log(`x dari foo(): ${x}`)
  }
  foo()
})()
console.log(x)
// x dari foo(): 1
// ReferenceError: x is not defined
{{< / highlight >}}

IIFE kependekan dari Immediately Invoked Function Expression dan fungsi yang langsung dieksekusi setelah didefinisikan. Dari kode di atas bisa kita lihat bahwa 
variable x tidak dapat diakses dari luar fungsi tetapi masih bisa diakses dari dalam fungsi walaupun dalam subfungsi.

### Lingkup blok

Lingkup blok didefinisikan dengan tanda kurung kurawal buka `{` dan tutup `}`.
