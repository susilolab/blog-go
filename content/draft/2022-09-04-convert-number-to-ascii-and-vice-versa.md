---
categories: javascript
date: "2022-09-04T05:00:00Z"
draft: true
title: Convert Number to ASCII and Vice Versa
---

Kadang kita butuh merubah dari angka ke karakter ascii, nah di nodejs kamus bisa menggunakan 2 fungsi ini `charCodeAt` untuk merubah dari karaketer ascii ke angka dan 
`fromCharCode` untuk merubah dari angka ke karakter ascii.
<!--more-->

Dari karakter ascii ke angka

{{< highlight javascript >}}
"\n".charCodeAt(0)
// 10
{{< / highlight >}}

Dari angka ke karakter ascii 

{{< highlight javascript >}}
String.fromCharCode(65)
// A
{{< / highlight >}}
