---
categories: php
date: "2022-08-24T05:00:00Z"
draft: true
title: PHP Short Arrow Function
---

Pada versi 7.4, PHP menambahkan fitur baru yaitu arrow function yang hampir sama seperti pada Javascript namun pada php hanya bisa satu baris belum mendukung multiline 
statement. Contoh:

{{< highlight php >}}
// sebelum 7.4
$numbers = [1, 2, 3];
$double = array_map(function ($x) {
    return $x * $x;
}, $numbers);

// setelah 7.4
$numbers = [1, 2, 3];
$double = array_map(fn ($item) => $x * $x, $numbers);
{{< / highlight >}}

Contoh lain penggunaan pada fungsi `array_reduce`:
<!--more-->

{{< highlight php >}}
$users = [
    ['id' => 1, 'username' => 'user1'],
    ['id' => 2, 'username' => 'user2']
];

// sebelum 7.4
$listData = array_reduce(
    $users,
    function ($acc, $user) {
        $acc = [...$acc, ...[$user['id'] => $user['username']]];
        return $acc;
    },
    []
);

// setelah 7.4
$listData = array_reduce(
    $users,
    fn ($acc, $user) => [...$acc, ...[$user['id'] => $user['username']]],
    []
);
{{< / highlight >}}

Dengan adanya arrow short function ini akan menghemat baris kode kita, semoga kedepan fungsi ini mendukung multiline statement.

sekian.
