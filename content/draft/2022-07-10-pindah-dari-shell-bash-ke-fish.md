---
categories: linux
date: "2022-07-10T06:00:00Z"
draft: true
title: Pindah dari shell Bash ke fish
---

![fish shell](/img/2022-07-10-fish-shell.png)

Kali ini saya ingin bercerita tentang kepindahan saya dari bash/zsh ke fish. Kenapa pindah dari bash ke fish?
<!--more-->

Alasan pertama yaitu karena fish menyediakan autocomplete berdasarkan history command, yang kedua adalah scriptingnya menurutku lebih mudah dan enak 
apalagi kalau background kamu adalah programmer.

### Instalasi

Untuk install fish cukup ketik command berikut jika kamu kamu pakai OS archlinux

`sudo pacman -S fish`

Untuk fedora 

`sudo dnf install fish`

Untuk ubuntu 

```
sudo apt-add-repository ppa:fish-shell/release-3
sudo apt-get update && sudo apt-get install
```

### Merubah default shell

Cek dulu daftar shell yang ada dengan command `chsh -l` 

```
/bin/sh
/bin/bash
/bin/zsh
/usr/bin/zsh
/bin/dash
/usr/bin/git-shell
/usr/bin/fish
/bin/fish
```

Kemudian ubah default shell ke fish dengan command berikut

`sudo chsh -s /bin/fish`

Jika hanya ingin merubah default shell untuk satu user yang aktif saja hilangkan `sudo` pada command di atas.

### Konfigurasi shell fish

Lokasi file config fish berada pada file `$HOME/.config/fish/config.fish`, berbeda dengan bash atau zsh yang berada pada `$HOME/.bashrc` dan `$HOME/.zshrc`.
Pada folder `$HOME/.config/fish` ini juga berisi config2 lain seperti autocomplete dan juga config plugins.

### Seting PATH

Untuk seting pada fish bisa menggunakan fungsi bawaan shell fish yaitu `set`, fungsi ini mirip dengan `export` pada shell bash/zsh. Namun tidak terbatas pada seting PATH,
fungsi ini juga untuk membuat variabel pada shell fish. Contoh cara seting PATH pada fish

```fish
set PATH "$HOME/.bin" $PATH
```

Agar setingan PATH dapat permanent maka perlu ditaruh pada file `.config/fish/config.fish` dan tambahkan opsi `-gx`. Contoh confignya:

```
set -gx VOLTA_HOME "$HOME/.volta"
set -gx SCCACHE_DIR "$HOME/.cache/sccache"
set -gx PATH "$VOLTA_HOME/bin" $PATH
```

### Alias command

Untuk menambahkan alias command juga dapat ditambahkan pada file `.config/fish/config.fish`. Contoh:

```
alias grep rg
alias cat bat
```

Config di atas akan menambahkan alias command `grep` dan akan menjalankan command `rg`, sedangkan untuk yang bawahnya akan menjalankan command `bat` pada 
saat user mengetik perintah `cat`.

### Refresh config

Pada shell bash untuk merefresh config kita biasa menggunakan command `source ~/.bashrc`. Untuk shell fish sendiri juga sama perintahnya untuk merefresh config yaitu 
dengan perintah `source ~/.config/fish/config.fish`.

### Set variabel

Seting variabel pada fish sama seperti pada saat seting path yaitu menggunakan perintah `set`. contoh:

```
set name Agus
echo $name
```

Jika nama mengandung spasi maka apit dengan petik dua

```
set name "folder test"
echo $name
```

Jika kita membuat folder dengan var $name maka fish akan membuatkan satu folder dengan nama "folder test" bukan 2 folder yaitu `folder` dan `test` seperti pada shell bash/zsh

```
set name "folder test"
mkdir tmp/$name
```

### Tema

Fish shell juga dapat dirubah tampilan sesuai selera kita, untuk itu kita bisa menginstall [oh-my-fish](https://github.com/oh-my-fish/oh-my-fish) agar pengaturan paket dan tema lebih mudah.

![bobthefish-theme](/img/ohmyfish-bobthefish.gif)

Untuk menginstall tema `bobthefish` pastikan ohmyfish sudah terinstall dan gunakan untuk menginstall paket dan tema. untuk menginstallnya ketik command berikut ini:

```
omf install bobthefish
```

Kemudian untuk menerapkan tema yang baru diinstall ketik 

```
omf theme bobthefish
```

Sekian dan terima kasih
