---
categories: rust
date: "2022-08-24T07:00:00Z"
draft: true
title: Ignore Unused Code Warning Pada Rust
---

Rust adalah bahasa pemrograman sistem yang cepat dan aman apalagi menyangkut variable yang tidak digunakan, Jika compiler Rust mendapati terdapat 
variabel, fungsi atau const maka pada saat running Rust akan memberikan peringatan kepada kita, hal ini akan membuat terminal kita banyak pesan warning yang tidak perlu.

Untuk menghilangkan peringatan ini kita bisa menggunakan 2 cara yaitu:
<!--more-->

Cara 1:

Kita bisa menambahkan syntax ini `#[allow(dead_code)]` tiap fungsi/variable.

{{< highlight rust >}}
#[allow(dead_code)]
fn hello() {
}

#[allow(dead_code)]
const PI: f64 = 3.14;

fn main() {
    #[allow(unused_variables)]
    let a = 1;
}
{{< / highlight >}}

Cara 2:

Kita bisa tambahkan syntax globalnya dengan menambahkan tanda `!` setelah tanda `#`.

{{< highlight rust >}}
#![allow(dead_code, unused_variables)]

fn main() {
    let a = 1;
}

fn hello() {
}
{{< / highlight >}}

sekian.
