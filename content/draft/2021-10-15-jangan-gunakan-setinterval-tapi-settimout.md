---
categories: javascript
date: "2021-10-15T05:00:00Z"
draft: true
title: Jangan gunakan setInterval tapi setTimeout!
---

Javascript memiliki 2 fungsi yang berguna untuk menjalankan blok fungsi pada interval tertentu. Yang pertama 
setInterval digunakan untuk menjalankan suatu fungsi pada setiap waktu misalnya tiap 1 detik. Namun jangan gunakan 
setInterval untuk kasus seperti berikut
<!--more-->

`setInterval` tidak cocok digunakan untuk fungsi yang membutuhkan waktu lama dalam menerima respon seperti ajax. 
karena misalnya kita memiliki fungsi dengan nama `getOrder` yang akan mengambil data pesanan pada tiap 2 detik, 
jika menggunakan `setInterval` pada request ke server mungkin membutuhkan waktu lebih dari 2 detik tetapi `setInterval` 
akan tetap menjalankan request baru setiap 2 detik tidak peduli data yang direquest itu sudah sampai ke client atau belum.

Perhatikan potongan kode berikut ini

{{< highlight javascript >}}
function getOder() {
    fetch('/get-order').then(resp => resp.json())
    .then(resp => setOrder(resp))
    .catch(err => console.log(err))
}

setInterval('getOrder', 2000)
{{< / highlight >}}

Bagaimana mengatasi kondisi di atas?. kita bisa gunakan `setTimeout` untuk mengganti fungsi dari `setInterval`. `setTimeout` akan 
menunda fungsi sampai waktu yang ditentukan pada parameter kedua pada fungsi `setTimeout`.

{{< highlight javascript >}}
function getOder() {
  fetch('/get-order').then(resp => resp.json())
    .then(resp => {
      setOrder(resp)
      getOrder()
    })
  .catch(err => console.log(err))
}

setTimeout('getOrder', 2000)
{{< / highlight >}}