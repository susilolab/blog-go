---
categories: javascript
date: "2019-10-01T06:00:00Z"
draft: true
title: Belajar Reactjs dan Vuejs
---

### Reactjs

Kali ini saya ingin belajar Reactjs dan Vuejs serta membandingkan seberapa cepat kita belajar React atau Vue. 
Perbandingan ini mungkin subjektif karena saya sudah familiar dengan Vue akan lebih condong ke Vue.
Walaupun library pertama kali yang saya pelajari adalah React.
Artikel ini saya tulis karena termotivasi teman saya yang sudah belajar React dan mengatakan seolah-olah belajar Vue itu lebih susah.
<!--more-->

Ok, langsung kita mulai saja. Pertama kita akan mencoba belajar React dulu membuat aplikasi todo sederhana
Pertama-tama buat proyek React dengan menggunakan `npx`.

```
$ npx create-react-app myreact
```

Dan kita akan mendapatkan struktur folder seperti ini

```
myreact
- node_modules
- package.json
- public
- README.md
--+ src
    - App.css
    - App.js
    - App.test.js
    - index.css
    - index.js
    - logo.svg
    - reportWebVitals.js
    - setupTests.js 
- yarn.lock
```

Dari struktur aplikasi ini yang akan kita perhatikan lebih hanya file `App.js` dan `index.js`,
karena bootstrap aplikasi ada pada file `index.js` dan satu komponen ada pada file `App.js`.

Kita bahas file index.js dulu

{{< highlight javascript >}}
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// reportWebVitals();
{{< / highlight >}}

Jika kita lihat struktur aplikasinya cukup sederhana dimana, pertama mengimport klas React, terus 
ReactDOM untuk merender komponen, import file `css` dan komponen `App`. Kemudian ReactDOM.render akan 
merender aplikasi ke dalam elemen dengan id `root`.

Kemudian buka file `App.js`


{{< highlight jsx >}}
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
{{< / highlight >}}

Dapat dilihat pada source code di atas bahwa React memisah file-file asset seperti css dan gambar terpisah 
dengan file komponen dan kita harus mengimportnya dengan syntax es6 module. Default komponen pada React disarankan menggunakan 
function daripada menggunakan class.

### Rendering data

Untuk merender data ke component React menggunakan syntax kurung kurawal `{data}`, didalamnya bisa berupa variabel, component, fungsi js atau statement

{{< highlight jsx >}}
function App() {
  return (
    <div className="App">
    {/* ... */}
    {myname}
    {/* ... */}
    </div>
  );
}
{{< / highlight >}}

Contoh component

{{< highlight jsx >}}
function App() {
  const button = <button>Hello</button>
  return (
    <div className="App">
    {/* ... */}
    {button}
    {/* ... */}
    </div>
  );
}
{{< / highlight >}}

Contoh fungsi

{{< highlight jsx >}}
function App() {
  return (
    <div className="App">
    {/* ... */}
    <button onClick={() => setCount(count + 1)}>Klik saya</button>
    {/* ... */}
    </div>
  );
}
{{< / highlight >}}

Contoh statement dan component

{{< highlight jsx >}}
function App() {
  const show = true

  return (
    <div className="App">
    {/* ... */}
    {show && <button>Klik saya</button>}
    {/* ... */}
    </div>
  );
}
{{< / highlight >}}

### Handle event

Untuk menghandle sebuah even klik, kita perlu membuat fungsi yang akan ditempatkan pada even `onClick`

{{< highlight jsx >}}
const handleClick = (e) => {
  e.preventDefault();
  console.log('Clicked');
}
{{< / highlight >}}

Kemudian letakan pada elemen yang akan dikenai event, misalnya link dan tempatkan event pada 
atribut `onClick` diikuti kurung kurawal.

{{< highlight jsx >}}
onClick={handleClick}
{{< / highlight >}}

Kode lengkapnya seperti ini

{{< highlight jsx >}}
function App() {
  const handleClick = (e) => {
    e.preventDefault();
    console.log('Clicked');
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
          onClick={handleClick}
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

{{< / highlight >}}

### Property

Kita dapat melewatkan data ke komponen anak menggunakan property. Buat file Hello.jsx sejajar dengan file App.js dengan
argumen `props` nama. Kemudian copas kode dibawah ini

{{< highlight jsx >}}
export default function Hello({ name }) {
  return (
    <>
      <h1>Hello {name}!</h1> 
    </>
  )
}
{{< / highlight >}}

Kemudian kita bisa import komponen Hello didalam file App.js dan letakan komponen Hello sebelum penutup div

{{< highlight jsx >}}
// ...
import Hello from './Hello'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

      <Hello name="Agus" /> {/* letakan disini */}
    </div>
  )
}
{{< / highlight >}}

Dan akan muncul tulisan `Hello Agus` pada browser

![Hello react](/img/001-belajar-vuejs.png)

### Conditional dan Looping

Conditional pada react menggunakan fungsi javascript dan tidak ada tambahan syntax baru seperti pada Vue. Ada 3 cara dalam conditional di React, 
pertama mengggunakan `if else` dan diletakkan sebelum return jsx, kedua menggunakan ternary if `exp ? stmt : stmt` dan yang ketiga menggunakan operator 
`&&` untuk if tanpa else.

{{< highlight jsx >}}
export default function Hello({ name }) {
  button = <button>Foo</button>
  if (name === 'Agus') {
    button = <button>Bar</button>
  }

  return (
    <>{button}</>
  )
}
{{< / highlight >}}

### State

Ok saya bingung bagaimana cara menaruh local state, mungkin kita harus merubah komponen dari function 
ke klas. Setelah mencari-cari di google ternyata kita bisa menggunakan Hooks. kita bisa mengimport 
fungsi `useState` dari module `react`.

Kita akan membuat component baru dengan nama `Counter` untuk menampilkan angka, button increment dan decrement

{{< highlight jsx >}}
export default function Counter () {
  const [count, setCount] = useState(0)

  return (
    <div>
      <p>Anda sudah mengklik {count} kali</p>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
    </div>
  )
}
{{< / highlight >}}
