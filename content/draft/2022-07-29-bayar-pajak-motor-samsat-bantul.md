---
categories: umum
date: "2022-07-29T05:00:00Z"
draft: true
title: Bayar pajak motor 5 tahunan di samsat bantul
---

![Samsat Bantul](/img/01-samsat-bantul.png)

Kali ini saya ingin berbagi prosedur pembayaran pajak 5 tahunan di samsat bantul yogyakarta.
<!--more-->

Prosedur pembayaran 

1. Setelah masuk pintu parkir langsung bawa motor dan ambil tiket pengecekan fisik di sebelah timur pintu parkir. (1)
2. Ambil formulir pengecekan fisik kendaraaan dan isikan sesuai dengan BPKB. Jangan lupa foto kopi BPKB 1 lembar, KTP/SIM/KK 1 lembar, dan STNK 1 lembar (2)
3. Setelah petugas selesai mengecek fisik kendaraan dan nomor rangka bawa formulir tersebut ke gedung sebelah barat pengecekan fisik kendaraan. (3)
4. Kemudian naik ke lantai (4) atas dan masukan formulir ke loket 3 (Pajak 5 tahunan). Ikuti prosedurnya sampai dapat STNK baru.
5. Lalu turun lagi ke gedung dimana kita menyerah formulir fisik kendaraan tadi dan antrikan kertas untuk pengambilan plat dan STNK baru (plastiknya dilepas dulu) (5). 

Selesai
