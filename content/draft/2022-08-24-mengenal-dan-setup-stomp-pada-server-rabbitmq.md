---
categories: web
date: "2022-08-24T05:00:00Z"
draft: true
title: Mengenal dan Setup Stomp pada Server RabbitMQ
---

![yii framework](/img/logo-rabbitmq.svg)

# Apa itu STOMP ?

STOMP adalah singkatan dari simple text-oriented message protocol. Ini mendefinisikan 
*interoperable wire format* jadi stomp client apapun dapat berkomunikasi dengan berbagai jenis 
stomp message broker dengan menyediakan pesan yang mudah dan dapat dioperasikan 
diantara bahasa dan platform.
