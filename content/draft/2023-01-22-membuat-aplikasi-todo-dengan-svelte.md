---
categories: javascript
date: "2023-01-22T05:00:00Z"
draft: true
title: Membuat Aplikasi Todo dengan Svelte
---

Svelte adalah pustaka untuk membuat web yang dibuat oleh Rich Harris. Nilai jual utama svelte yaitu tidak menggunakan virutal dom (VDOM) untuk memonitor perubahan data dan
mengkompilasi ulang html. Svelte akan mengkompile komponen html menjadi native javascript sehingga bisa meniadakan adanya runtime, hal ini akan membuat ukuran aplikasi
menjadi lebih kecil dibanding pustaka yang menggunakan vdom dan juga performanya jauh lebih cepat.

<!--more-->

{{< highlight svelte >}}

<script>
  let msg = 'hello world'
</script>

<div>{msg}</div>
{{< / highlight >}}
