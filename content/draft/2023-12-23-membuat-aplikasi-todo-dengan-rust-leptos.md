---
categories: php
date: "2023-12-23T07:00:00Z"
draft: true
title: Scaling PHP Application
---

Hari ini saya ingin ngobrol tentang performa aplikasi yang dibuat dengan bahasa `PHP` karena ditempat kerja 
saya sedang membicarakan tentang berapa banyak pengguna yang dapat online sekaligus pada aplikasi kami. Benarkah aplikasi **PHP** bisa discale up?
<!--more-->

Rekan tim kemudian 
membenchmark/mengukur performa dengan tool `apache benchmark` (`ab`) pada halaman landing page saja. Dengan jumlah request 
20k dan concurrentnya 20k lebih dari itu tool benchmark tidak mau jalan. Namun untuk mengetahui berapa jumlah maksimal 
user yang dapat online pada web tidak sesederhana itu. Ada banyak parameter lain yang harus dipertimbangkan.
Mengutip dari `servebolt.com` bahwa mengukur jumlah maksimal user yang dapat online harus memperhatikan beberapa parameter 
seperti berapa lama user online pada tiap sesi, rata-rata user mengklik tiap detik, jumlah maksimal request per detik dan
parameter tersebut bisa kita dapatkan dari google analytic.

Dari data tersebut baru kita bisa mengetahui jumlah maksimal user yang dapat online pada web kita yaitu dengan 
mengkalikan maksimal request perdetik dikali 60, dikali klik frekuensi user perdetik maka akan didapat maksimal user 
yang dapat online secara simultan. Lalu bagaimana cara meningkatkan performa web kita?

### Meningkatkan performa aplikasi PHP

Ada beberapa cara meningkatkan performa web atau istilahnya optimalisasi yaitu
1. Mengoptimalkan query `select`. Query yang yang tidak optimal akan memperlambat akses web, hal yang harus adalah 
hindari penggunaan `select *` pada saat query ke database. Jika tidak membutuhkan semua kolom maka sebutkan 
nama kolom pada saat select.
2. Gunakan index pada tabel secara tepat sesuai kebutuhan. Index pada tabel dapat mempercepat proses query database.
3. Gunakan cache pada data yang sering diakses. Cache dapat mempercepat akses data tanpa perlu query ke database. 
Tool yang bisa digunakan di antaranya `Redis` atau `Memcached`.
4. Menyederhanakan dan mengoptimalkan kode. Kode yang tidak optimal dan komplek akan sangat berpengaruh pada kecepatan 
karena terlalu banyak logic yang tidak perlu.
5. Menambah ram, cpu atau bahkan jumlah server. Hal ini merupakan pilihan terakhir jika optimasi dari sisi software 
sudah maksimal.

### Penggunaan Job Queue, Background Process, Worker

Jika kode sudah dioptimalkan, `query` juga sudah dibuat seminimal mungkin dan sudah menggunakan teknik `cache` namun proses masih 
lambat maka perlu adanya pemisahan proses yang berat ke dalam `job queue` atau `background proses`

### Penggunaan Teknologi Async

Penggunaan async dapat membantu mempercepat proses karena fungsi dapat dieksekusi secara concurent dan non blocking, itu artinya suatu proses 
dapat ditunda dan dilanjut eksekusi berdasarkan dari penjadwalan yang disebut dengan `event loop`. Untuk **PHP** merubah fungsi-fungsi pada aplikasi yang sudah berjalan 
dengan fungsi async akan membutuhkan waktu dan sumber daya yang tidak sedikit.  

Karena pada dasarnya **PHP** tidak dirancang dari awal untuk melakukan sesuatu secara async, bawaan **PHP** semua fungsi berjalan secara berurutan. Oleh karena menerapkan teknologi async 
akan butuh waktu yang tidak sebentar dan pembiasaanya dengan event loop. Tetapi pada versi 8.1 php mulai menyertakan fitur dasar yang akan memudahkan penggunakan async pada fungsi yaitu
`fiber`, **fiber** memungkinkan suatu fungsi dapat di pause dan diresume.  

Keuntungan lain dengan adanya fiber ini adalah memungkinkan kode async bisa terlihat seperti sinkronous kode dan pustaka seperti `amphp` dan `reactphp` sudah menerapkan fiber pada pustakanya.

Jika penggunaan pustaka async dirasa masih kurang cepat maka perlu menginstall modul php seperti `event`, `ev` atau `uv`, modul ini mirip dengan event loop yang dipake pada nodejs.

Pilihan terakhir adalah bisa menggunakan teknologi `openswoole` atau `swoole`, teknologi ini paket lengkap karena banyar fitur yang ditawarkan seperti event driven, coroutine (mirip goroutine),
websocket server, grpc, multiprocessing dll. Namun modul ini membutuhkan kompilasi waktu yang cukup lama.

### Hybrid System

Hybrid sistem artinya menambah bahasa aplikasi baru dan itu artinya butuh programmer lagi yang paham bahasa baru tersebut, seperti Go, Rust atau Nodejs.

### Penggunaan GRPC
