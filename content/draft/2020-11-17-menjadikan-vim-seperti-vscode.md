---
categories: linux
date: "2020-11-17T07:00:00Z"
draft: true
title: Menjadikan Vim seperti Vscode
---

Beberapa hari ini saya mencoba koding menggunakan editor vim dan berhasil mensetingnya menjadi seperti
sublime text atau vscode. Hasilnya wow banget serasa ndak pengen pindah text editor.
Karena ternyata vim juga bisa autocomplete menggunakan LSP(Language Server Protocol).
Saya ingin berbagi pengalaman saat menseting vim sehingga bisa lebih powerful dan nyaman digunakan.
<!--more-->

Pertama-tama install neovim, saya menggunakan versi 0.4 atau bisa juga menggunakan vim 8. 
Pada contoh ini saya menggunakan neovim, untuk vim konfigurasinya agak berbeda.
Install dengan menggunakan terminal

## Archlinux

{{< highlight bash >}}
$ sudo pacman -S neovim
{{< / highlight >}}

Ubuntu

{{< highlight bash >}}
$ sudo apt-get install neovim
{{< / highlight >}}

Jika ingin mendapatkan versi stable yang terakhir bisa menambah repo terlebih dulu

{{< highlight bash >}}
$ sudo add-apt-repository ppa:neovim-ppa/stable
$ sudo apt-get update
$ sudo apt-get install neovim
{{< / highlight >}}

## Konfigurasi

Neovim sedikit berbeda dalam hal nama file untuk seting dan lokasinya, neovim menyimpan 
konfigurasi di folder `$HOME/.config/nvim` dan nama filenya `init.vim`. Kalau di vim 
nama file setingnya `.vimrc` dan terletak di folder `$HOME/.vimrc`.

## Plugin Manager

Agar bisa lebih mudah mengelola plugin seperti install dan uninstall. Saya menggunakan 
`vim-plug` karena sudah terbiasa, temen-temen bisa menggunakan plugin manager lain 
seperti `pathogen`, `vundle` atau yang lainnya.

Untuk menginstall `vim-plug` bisa menggunakan command seperti berikut sesuai intruksi 
pada repo `vim-plug`.

{{< highlight bash >}}
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
{{< / highlight >}}

Setelah itu edit file di `$HOME/.config/nvim/init.vim` dan tambahkan kode berikut.
{{< highlight vim >}}
call plug#begin('~/.local/share/nvim/plugged')
    " Taruh plugin2 di sini
    " contoh:
    Plug 'jiangmiao/auto-pairs'
call plug#end()
{{< / highlight >}}

Reload `init.vim` dan ketik `:PlugInstall` untuk menginstall plugin. Mulai dari sini 
kita sudah bisa menginstall plugin-plugin yang akan membuat vim serasa vscode.

Autocomplete dengan `coc.vim`

Pertama kita akan menginstall plugin [`coc.vim`](https://github.com/neoclide/coc.nvim) yang akan membuat vim dapat 
memberikan saran (autocomplete) pada kode yang tulis. Lihat demo pada gambar di bawah ini:

![grub2](/img/20201121-coc-vim.gif)

`coc.vim` membutuhkan nodejs versi 10.12 ke atas, Install dulu nodejs kemudian tambahkan `coc.vim` 
ke daftar plugin pada file `init.vim`.

{{< highlight vim >}}
Plug 'neoclide/coc.nvim', {'branch': 'release'}
{{< / highlight >}}

Kemudian reload file `init.vim` dan ketik `:PlugInstall` untuk menginstall plugin.
