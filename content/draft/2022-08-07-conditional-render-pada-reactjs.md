---
categories: javascript
date: "2022-08-07T05:00:00Z"
draft: true
title: Conditional render pada Reactjs
---

Jika ingin render component berdasarkan kondisi tertentu kamu dapat menggunakan `if else` atau menggunakan ternary `exp ? statement : statement`. If dapat digunakan 
jika component tidak berada pada jsx, berikut contoh penggunaanya.
<!--more-->

### Dengan if

Karena if tidak dapat digunakan didalam jsx maka variabelnya kita taruh didalam variabel sebelum return template seperti pada contoh kode dibawah

{{< highlight jsx >}}
function Button({ newRecord }) {
  let button = <button>Tambah</button>
  if (newRecord) {
    let button = <button>Simpan</button>
  }

  return (
    <>{button}</>
  )
}
{{< / highlight >}}

### Dengan ternary

Dengan ternary kita bisa langsung taruh didalam jsxnya dan hal ini sangat umum dilakukan oleh developer React

{{< highlight jsx >}}
function Button({ newRecord }) {
  return (
    <>{newRecord ? <button>Tambah</button> : <button>Simpan</button>}</>
  )
}
{{< / highlight >}}

Jika kondisinya ada else-nya maka kita bisa gunakan operator `&`

{{< highlight jsx >}}
function Button({ newRecord }) {
  return (
    <>{loading & <div>Tambah</div>}</>
  )
}
{{< / highlight >}}

Jika isi dari templatenya banyak maka kita bisa pindah komponen tersendiri agar lebih mudah dikelola. Sekian dan semoga bermanfaat.
