---
categories: linux
date: "2022-07-30T05:00:00Z"
draft: true
title: Monitoring service systemd dengan watch
---

![Systemd](/img/systemd-logo.svg)

Biasanya jika ingin melihat status dari service systemd kita akan menggunakan perintah `systemctl status <nama-service>`. Bagaimana bila 
ingin terus menerus melihat status dari sebuah service?.
<!--more-->

Kita bisa gunakan command `watch` sebelum command di atas yaitu seperti ini

```
watch systemctl status nginx
```

Maka kita akan dapat status dari service `nginx` setiap ada perubahan status.

Sekian semoga bermanfaat
