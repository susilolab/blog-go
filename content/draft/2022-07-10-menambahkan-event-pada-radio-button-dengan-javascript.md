---
categories: javascript
date: "2022-07-10T06:00:00Z"
draft: true
title: Menambahkan event pada radio button dengan javascript
---

Assalamu'alaikum, kali ini saya ingin berbagi tip bagaimana menambahkan event pada radio button dengan javascript murni tanpa bantuan library lain.
<!--more-->

Jika menggunakan jQuery hal ini akan sangat mudah tinggal panggil fungsi dolar dan fungsi on seperti ini 
{{< highlight javascript >}}
$('input[name="radio-langs"]').on('click', function (evt) {
  console.log(evt)
})
{{< / highlight >}}

Namun jika kita menggunakan javascript murni scriptnya akan sedikit panjang dan karena biasanya inputan seperti radio button dan 
checkbox button jumlahnya lebih dari satu maka harus menggunakan fungsi `querySelectorAll`. Contoh scriptnya 
seperti ini

{{< highlight javascript >}}
document.addEventListener('DOMContentLoaded', function (ev) {
  const radioLangs = document.querySelectorAll('input[name="radio-langs"]')
  radioLangs.forEach((elm) => {
    elm.addEventListener('click', (e) => {
      console.log(e.target.value)
    })
  })
})
{{< / highlight >}}

Contoh dokumen htmlnya

{{< highlight html >}}
<input type="radio" name="radio-langs" value="PHP">PHP<br />
<input type="radio" name="radio-langs" value="Javascript">Javascript<br />
{{< / highlight >}}

Dapat dilihat pada kode di atas sebelum menambah event untuk radio kita menambah event untuk `DOMContentLoaded` lebih dulu, hal ini mirip 
seperti `$(document).ready` nya jQuery yaitu untuk menunggu sampai semua dokumen html selesai dimuat.

Jika dijalankan script di atas akan mencetak pada console jika memilih PHP akan muncul PHP dan jika Javascript akan muncul Javascript.

Sekian semoga bermanfaat.
