---
categories: javascript
date: "2022-05-27T06:00:00Z"
draft: true
title: 3 Cara Deklarasi Variable di Javascript
---

Pada bahasa pemrograman deklarasi variabel umum dilakukan untuk menyimpan data tertentu yang akan diolah. 
pada Javascript terdapat 3 cara deklarasi variabel setelah javascript versi 2015 atau dikenal dengan ecmascript6(es6).
<!--more-->

### var

Keyword `var` adalah cara satu-satunya mendeklarasikan variabel sebelum es6, namun setelah es6 terdapat 2 keyword tambahan 
yaitu `let` dan `const`. `var` akan mendeklarasikan variabel yang bersifat global atau lingkup fungsi, jika dideklarasikan didalam/diluar scope.

{{< highlight javascript >}}
{
  var x = 5
  console.log(x)
}
console.log(x)

function hello() {
  var y = 1
  {
    var y = 2 // variabel yang sama
    console.log(`y scoped: ${y}`) // y scoped: 2
  }
  console.log(`y scoped fungsi: ${y}`) // y scoped fungsi: 2
}
hello()
{{< / highlight >}}

Kurung kurawal (`{}`) pada sebelum dan sesudah kode adalah untuk membatasi scope/lingkup.

### let

Keyword `let` fungsinya sama yaitu untuk mendeklarasikan variabel, namun terdapat perbedaan antara `var` dan `let` yaitu `let` berbasis scope/lingkup. 
Pada modern javascript kita disarankan menggunakan keyword `let` dan `const` saja. Perbedaan `let` dengan `var` adalah lingkup aksesnya, jika mendeklarasikan variabel 
dengan let didalam scope maka variabel tersebut tidak dapat diakses diluar scope. Jika variabel dengan let didalam scope maka variabel tersebut 
tidak dapat diakses dari luar scope/didalam scope bersarang.

Untuk lebih memahami penjelasan di atas maka kita bisa lihat kode dibawah:

{{< highlight javascript >}}
{
    let x = 10
    console.log(x)
}
// error ReferenceError: x is not defined
// console.log(x)

function hello() {
  let x = 1
  {
    let x = 2 // variabel yang berbeda
    console.log(x) // 2
  }
  console.log(x) // 1
}
hello()
{{< / highlight >}}

Jadi variable yang dideklarasikan dengan let akan terbatas pada scope(`{}`) dan tidak dapat diakses diluar scope tersebut meskipun scope tersebut bersarang.
contoh scope bersarang yaitu pada fungsi hello dimana terdapat variabel `x` yang bernilai 1 dan 2. Walaupun namanya sama 
namun karena yang satu ada dalam scope fungsi dan yang satunya ada dalam scope lain maka tidak dapat diakses.

Jika kita memaksa akses variabel tersebut maka akan error exception `ReferenceError: Cannot access 'x' before initialization`.

{{< highlight javascript >}}
function hello() {
  let x = 1
  {
    // mencoba akses var x pada scope di atasnya
    // error: ReferenceError: Cannot access 'x' before initialization 
    console.log(x) 
    let x = 2 // variabel yang berbeda
    console.log(x) // 2
  }
  console.log(x) // 1
}
hello()
{{< / highlight >}}

Deklarasi variabel dengan `var` dan `let` sama-sama dapat diassign kembali nilainya tetapi `let` tidak mengijinkan 
pendeklarasian ulang variable dengan nama yang sama.

{{< highlight javascript >}}
var x = 10
var x = 5

let y = 10
// Uncaught SyntaxError: Identifier 'y' has already been declared
let y = 5 
{{< / highlight >}}

### const
