---
categories: php
date: "2023-12-10T05:00:00Z"
draft: true
title: Mencari bugs tanpa menjalankan dengan phpstan
---

Mengelola aplikasi php jadul adalah hal yang mungkin membosankan karena dari versi phpnya sampe paket-paket sudah usang apalagi jika phpnya masih versi 5.6 haduh, belum lagi bugs yang ada karena aplikasi dikembangkan turun 
temurun dengan berbagai latar developer yang berbeda. Untuk cara upgrade php dari versi 5.6 ke 8 Insha Allah akan saya bikinkan artikelnya.

Namun jangan khawatir para developer php sudah membuat banyak tool untuk mengatasi hal tersebut, salah satu tool yang ingin saya kenalkan adalah [**PHPStan**](https://phpstan.org) sebuah static analysis yang dapat 
membantu menscan kode php tanpa running diproduction untuk mencari bugs yang ada.

![logo-phpstan](/img/2023-12-10-logo-phpstan.png)

Nah kali ini saya ingin berbagi cara penggunaan **PHPStan**
<!--more-->

Didalam gRPC aplikasi client dapat langsung memanggil fungsi di server pada mesin yang berbeda seperti jika fungsi tersebut adalah objek lokal, hal ini membuatmu lebih mudah 
untuk membuat aplikasi dan servis yang terdistribusi.  

![demo-phpstan](/img/2023-12-10-demo-phpstan.png)
Gambaran tentang gRPC bisa dilihat pada diagram dibawah ini.  

![diagram-grpc](/img/grpc-overview.svg)

Dapat dilihat pada gambar bahwa server grpc dibuat dengan bahasa c++ sedangkan 2 client menggunakan Ruby dan Java. Hal ini bisa dilakukan karena masing-masing kode baik 
server maupun client memiliki interface yang sama hasil dari file `.proto`. masing-masing dari server dan client akan digeneratekan kode berdasarkan file `.proto` 
sehingga walaupun hasil generate kodenya berbeda bahasa tetap bisa berkomunikasi.

Dan kali ini saya ingin mengenalkan cara membuat server grpc dengan Rust dan clientnya dengan PHP. Mengapa memilih menggunakan Rust?. Pertama karena saya ingin lebih mendalami Rust, 
yang kedua adalah karena faktor safety (keamanan) bukan kecepatan. Karena di Rust kalau sudah bisa terkompile Insha Allah kode kita aman hehehe.  

Kemudian kenapa untuk client pake PHP karena bahasa ini sudah lama banget saya pake dari saya SMK dan juga tempat saya bekerja masih menggunakan PHP sebagai bahasa utama walaupun 
sekarang sudah campur dengan bahasa lain.  

Jadi saya ingin menguji gRPC ini ditempat saya bekerja agar kode baru pada fitur baru tidak terlalu semrawut.  

Sebelum kita ngoding saya ingin memberikan daftar kebutuhan yang harus diinstall di lokal development baik Rust maupun PHP.

* **protoc** untuk mengenerate stub kode khusus di PHP, karena di Rust tidak perlu protoc ini.
* grpc php plugin 
