---
categories: go
date: "2022-08-27T05:00:00Z"
draft: true
title: Memahami Channel Go
---

Channel di Go merupakan cara untuk berkomunikasi antara satu goroutine dengan goroutine lain. Channel defaultnya unbuffer yang artinya pengiriman dan penerimaan 
harus dilakukan saat itu juga, karena pada saat dibuat channel tidak punya ruang/space maka kita tidak bisa melakukan satu operasi saja misalnya read saja atau write saja. 
Channel juga merupakan tipe bawaan Go, untuk membuat channel kita bisa menggunakan fungsi `make` diikuti keyword `chan` dan tipe data.

<!--more-->

Untuk unbuffer channel kita butuh minimal 2 goroutine untuk mengirim dan menerima karena pada saat menulis ke channel akan ada penundaan sampai channel tersebut 
dibaca. Jika kita melakukan salah satu operasi read/write saja maka akan menyebabkan deadlock.

Perhatikan kode dibawah:

{{< highlight go >}}
package main

import (
    "fmt"
)

func main() {
    num := make(chan int)
    go write(num, 1)
    go read(num)

    var input string
    fmt.Scanln(&input)
}

func read(recv <-chan int) {
    fmt.Println(<-recv)
}

func write(send chan<- int, num int) {
    send <- num
}
{{< / highlight >}}
