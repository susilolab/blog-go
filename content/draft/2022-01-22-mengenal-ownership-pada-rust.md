---
categories: rust
date: "2022-01-22T06:00:00Z"
draft: true
title: Mengenal Ownership pada Rust
---

Rust adalah bahasa pemrograman sistem itu artinya ia sangat memperhatikan performa dan keamanan dan juga dapat diletakan pada devices yang sangat kecil 
misalnya microcontroller atau yang sejenisnya. Kendala untuk para pemula dalam belajar Rust adalah memahami konsep baru dalam pemodelan memori yang saya pikir 
baru di Rust saja konsep ini diterapkan. Contoh pada bahasa C/C++ yang memberikan kebebasan dalam pengalokasian memori tetapi menyerahkan tanggung jawab dalam 
pembebasan memori kepada programmer/user. Namun hal ini seringkali bukan pekerjaan yang mudah mengingat manusia itu sering salah. Seperti pada laporan yang dirilis oleh 
Microsoft bahwa lubang keamanan yang sering terjadi terdapat pada keamanan memori. Kita tahu bahwa pada C/C++ kita sering mendengar tentang buffer overflow, dangling memori, 
use after free dsb.
<!--more-->

Maka untuk mengatasi masalah itu Rust menggunakan konsep ownership, borrowing dan lifetimes. Konsep ini menjadi kendala utama oleh pemula yang belajar Rust karena konsepnya yang baru.
Nah kali ini saya ingin berbagi sedikit mengenai ownership pada Rust agar teman-teman sedikit terbantu dalam memahami konsep ownership ini.

Ownership pada Rust memberikan aturan bahwa suatu nilai hanya boleh dimiliki oleh satu pemilik, ia bisa dipinjam atau berpindah kepemilikan. Misalnya kita punya buku, buku kita bisa 
kita pinjamkan atau kita berikan kepada pemilik baru. Ada tiga aturan pada *ownership* yaitu

* Setiap nilai di Rust memiliki variabel yang disebut pemilik(owner)
* Hanya ada satu pemilik pada satu waktu
* Saat pemilik keluar dari lingkupnya(scope), nilai akan dihapus(dropped)

Agar lebih mudah memahami mari kita lihat contoh potongan kode berikut

{{< highlight rust >}}
fn main() {
    let a = 10;
    {
        let b = a;
        println!("{}", b);
    } // <-- (1)
    println!("{}", b);
} // <-- (2)
{{< / highlight >}}

Pada potongan kode di atas ada dua scope yaitu scope satu dimana variabel b dideklarasikan dan scope dua dimana variabel a dideklarasikan. 
Variabel a valid sampai akhir scope pada penutup (`}`) fungsi main, sedangkan variabel b valid sampai scope satu berakhir. Untuk lebih memahami aturan *ownership* 
mari kita gunakan tipe `String` yang sangat berhubungan sekali dengan ownership dari pada tipe `integer` ataupun `str`.

{{< highlight rust >}}
fn main() {
    let s = String::from("hello");
}
{{< / highlight >}}
