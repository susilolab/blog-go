---
categories: elixir
date: "2020-08-07T07:00:00Z"
draft: true
title: Menggunakan vapor untuk konfigurasi aplikasi elixir
---

Assalamu'alaikum teman-teman, kali saya ingin sedikit berbagi tentang [Vapor](https://github.com/keathley/vapor). Bagi yang belum tahu, vapor adalah pustaka untuk mengatur konfigurasi pada aplikasi yang dibuat dengan bahasa Elixir.

Kenapa menggunakan vapor? Pertama, karena saya mengalami sesuatu yang tidak biasa pada saat mendeploy aplikasi di server production sehingga mengharuskan saya untuk meggunakan config lain yang bukan official. Kedua karena vapor lebih fleksibel, vapor dapat membaca konfigurasi dari beberapa provider seperti `.env`, file json, toml dan yaml bahkan juga dapat membaca dari variabel environmen.

Ok langsung saja, untuk menggunakan vapor pertama tambahkan library ke daftar dependency di file `mix.exs`

{{< highlight elixir >}}
...
  defp deps do
    [
      ...
      {:vapor, "~> 0.9"}
    ]
  end
...
{{< / highlight >}}

Setelah library sudah kita tambahkan ke file `mix.exs`, langkah selanjutnya adalah menggunakan vapor pada bagian bootstrap. Aplikasi elixir yang digenerate mix dengan tambahan opsi `--sup` akan memiliki file `nama_app/lib/nama_app/application.ex`. Nah kita akan menggunakan vapor pada file tersebut.

Pada contoh pertama ini kita akan membaca konfig dari file `.env`. Sebelumnya buat file `.env` terlebih dahulu file pada root folder aplikasi.
Isinya seperti ini:

{{< highlight bash >}}
DB_NAME=myapp
DB_USER=root
DB_PASS=
DB_HOST=localhost
{{< / highlight >}}

Untuk mengurangi pengetikan sebaiknya kita gunakan keyword `use` untuk memperpendek nama modul.
{{< highlight elixir >}}
defmodule Myapp.Application do
  use Vapor.Provider.Dotenv
  ...
end
{{< / highlight >}}

Kemudian buat binding dengan `Dotenv` karena kita akan menggunakan file `.env` untuk menyimpan konfigurasi. Lalu simpan hasil load konfig ke variabel `config`. 

{{< highlight elixir >}}
defmodule Myapp.Application do
  use Vapor.Provider.Dotenv
  ...
  def start(_type, _args) do
    providers =[
      %Dotenv{bindings: [
        {:db_name, "DB_NAME"},
        {:db_user, "DB_USER"},
        {:db_pass, "DB_PASS"},
        {:db_host, "DB_HOST"}
      ]}
    ]
    config = Vapor.load!(providers)
  end
  ...
end
{{< / highlight >}}

Sampai di sini kita sudah bisa menggunakan hasil load konfigurasi vapor yang sudah disimpan pada variabel `config`, misal butuh nama database tinggal panggil `config.db_name`.
Misalnya kita bisa menggunakan pada saat modul `Repo` akan diset ke supervisor. Lihat contoh berikut ini:

{{< highlight elixir >}}
defmodule Myapp.Application do
  ...
  def start(_type, _args) do
    ...
    children = [
      {Myapp.Repo, [
        database: config.db_name,
        username: config.db_user,
        password: config.db_pass, 
        hostname: config.db_host
      ]},
      ...
    ]
    ...
  end
  ...
end
{{< / highlight >}}

Pada contoh kedua kita akan menggunakan file json sebagai tempat untuk menyimpan konfigurasi aplikasi. 
Sebelumnya buat dulu file jsonnya misal namanya `config.json` lalu simpan sejajar dengan folder `lib`.
Kemudian copy paste kode berikut ini.
{{< highlight json >}}
{
  "db_name": "db_name",
  "db_user": "username",
  "db_pass": "password",
  "db_host": "localhost"
}
{{< / highlight >}}

Kemudian masuk ke kode elixir, ganti use modul `Dotenv` menjadi `File`.

{{< highlight elixir >}}
defmodule Myapp.Application do
  use Vapor.Provider.File
  ...
  def start(_type, _args) do
    providers =[
      %File{
        path: System.get_env("MYAPP_PATH") <> "config.json",
        bindings: [
          {:db_name, "db_name"},
          {:db_user, "db_user"},
          {:db_pass, "db_pass"},
          {:db_host, "db_host"}
        ]
      }
    ]
    config = Vapor.load!(providers)
  end
  ...
end
{{< / highlight >}}

Setelah fungsi `Vapor.load!()` dipanggil dan dimasukkan ke variabel `config` maka kita bisa menggunakan
config tersebut kedalam definisi children supervisor.

{{< highlight elixir >}}
defmodule Myapp.Application do
  use Vapor.Provider.File
  ...
  def start(_type, _args) do
  ...
    children = [
      ...
      {Myapp.Repo, [
          database: config.db_name,
          username: config.db_user,
          password: config.db_pass, 
          hostname: config.db_host
        ]
      }
      ...
    ]
  ...
  end
end
{{< / highlight >}}

Untuk menjalankan aplikasi kita perlu menset variabel shell `MYAPP_PATH` sesuai dengan lokasi aplikasinya.
Dengan menggunakan config file membuat aplikasi kita akan lebih mudah dalam mengganti konfigurasinya.

Sekian tips dari saya semoga bermanfaat.
