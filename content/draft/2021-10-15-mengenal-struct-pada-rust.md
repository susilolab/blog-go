---
categories: rust
date: "2021-10-15T05:00:00Z"
draft: true
title: Mengenal struct pada Rust
---

Di Rust tidak deklarasi klas seperti pada bahasa pemrograman OOP seperti Java, PHP, C# atau yang sejenisnya karena 
konsep OOP berbeda mungkin tidak ada tetapi diganti dengan `struct` dan `trait`.
<!--more-->
