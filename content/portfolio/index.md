+++
date = '2023-05-06'
description = 'Portfolio'
slug = 'portfolio'
title = 'My Portfolio'
+++

1. #### SIMAK Merapi
GIS based application to help recovery plan and action due Merapi vulcano disaster. My task is to show
the bubble of aid post and provide a form to input the report.

2. #### One Village One Product (OVOP)  
This web is to facilitate each village to produce one unique product to attract foreign tourists to come to
their village. My task is to create form input, login, etc.

3. #### Engineering Career Center website v2 for ECC UGM  
Totally rebuilding from 1 st version due to the growth of ECC member, it’s first time ECC using Yii
framework version 1 for their web. My task is to create form for jobseeker, employer, search,
registration, sending email to member, to tweak the query of most important part, etc.

4. #### Gadjah Mada Medical E-Learning (GaMEl)  
Website to provide e-learning system for medical student especially interested in pharmacology. The
supplement of extra curiosity of any student : online quiz, discussion board, e-book download and even
animation video. My task is to create the whole system except frontend.

5. #### UGM Landslide Participatory and Early Warning System  
UGM Landslide Participatory and Early Warning System is information system that receive data from
participants at the field arround of landslide such as rain code, symptoms, vulnerability, azimuth and
coordinate. This system will then process the data into a decision.
My task is to create form for user to be able to report, to show dialog about landslide, I also create
search report based on region. Also my task is to process the report that sent from mobile messaging,
this is actually PHP cron job using kannel.

6. #### Sweeto Platform  
This is non visual application that help our developer to develop application faster and easier, this
platform can manage user rights using RBAC(adding role, assign role to user/group), manage menu,
manage user, etc. My task to design and develop how configuration of system can be loaded from the
database, and how the module can be loaded easily, also to develop dynamic themes and manage user
permission using RBAC.

7. #### Sistem Basis Data Terpadu Teknik Geodesi UGM  
Application to help college student to submit their thesis submission, to help lecturer to manage their
student conselling, progress the thesis and so on until college student complete their study. My task is
to create management page for student, lecturer, staff, etc. also my task is to create management page
for history of student and lecturer such as manage their biodata, training, scholarship and so on.

8. #### IdeaConnect  
This application is similar to indiegogo.com, kickstarter.com, patreon.com except it’s managed by ECC
UGM. My task is to create input form for Iniate Project including the wizard, showing the project,
search project, etc.

9. #### Food Ordering System (SweetFOS)  
SweetFOS is a software package consist of backoffice (web & mobile responsive view ready), cashier
(web), checker page (web), and waitress app (andoid apk) especially for restaurant/food court.
Although it's developed especially for restaurant/food court, but it can also be used for store. We've
installed it to WaroengSS-Spesial Sambal (some with customs).
My task is to create payment, show menu in front of order, process the order, to develop sweetfos
installer for windows user, to develop about license, etc.

10. #### [ecc.co.id](https://ecc.co.id)
**ecc** merevolusi cara pengembangan diri dan menghasilkan talent berkualitas dengan menyediakan platform jejaring online yang menghubungkan para profesional dan kandidat pencari kerja.

